# -*- coding: utf-8 -*-
{
    'name': "Invas Connector",

    'summary': "Synchronize data between Odoo and Invas",

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Warehouse',
    'version': '12.0.0.0.0',

    'depends': [
        'sale_management',
        'purchase',
        'stock',
        'base_automation',

        'l10n_cl_fe',
    ],

    'data': [
        'data/ir_module_category_data.xml',
        'data/base_automation.xml',
        'data/ir_cron.xml',
        'security/invas_security.xml',
        'security/ir.model.access.csv',
        'views/invas_instance_views.xml',
        'views/invas_log_views.xml',
        'views/invas_url_views.xml',
        'views/invas_location_views.xml',
        'views/invas_out_order.xml',
        'views/invas_asn.xml',
        'views/purchase_views.xml',
        'views/invas_inventory_type.xml',
        'views/invas_storage_seq_views.xml',
        'views/invas_route_views.xml',
        'views/sale_views.xml',
        'views/res_partner_views.xml',
        'views/product_views.xml',
        'views/stock_views.xml',
        'views/invas_menu.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
