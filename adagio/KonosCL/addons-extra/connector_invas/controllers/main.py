# -*- coding: utf-8 -*-

import itertools
import json
from odoo import http
from odoo.http import request, JsonRequest, Response


class InvasController(http.Controller):

    @http.route(
        '/invas/asn', methods=['POST'], type='json', auth='public', csrf=False)
    def handler(self):
        """Allows us to receive the information of all completed ASNs that
        were created in Invas through a transfer operation in Odoo."""
        charset = request.httprequest.charset
        data = json.loads(request.httprequest.data.decode(charset))

        if data and data.get('listaCajas'):
            source = data.get('sitio')
            order = data.get('os_transferencia')
            number = data.get('idasn')
            domain = [('transfer_order', '=', order), ('number', '=', number)]
            exists = request.env['invas.asn'].sudo().search(domain)

            if not exists:
                lines = []
                for key, group in itertools.groupby(
                        data.get('listaCajas'), lambda r: r.get('sku')):
                    items = list(group)
                    values = {
                        'sku': key,
                        'quantity': sum([
                            int(item.get('unidades')) for item in items]),
                        'lpn': '/'.join([
                            item.get('idlpn') for item in items]),
                    }
                    lines.extend([(0, 0, values)])

                location = request.env['invas.location'].sudo().search([
                    ('name', '=', source)])
                if location:
                    asn = request.env['invas.asn'].sudo().create({
                        'company_id': location.company_id.id,
                        'name': data.get('idtrx'),
                        'source': source,
                        'number': data.get('idasn'),
                        'transfer_order': data.get('os_transferencia'),
                        'asn_line': lines,
                    })
                    return json.dumps({"status": "success", "message": "insercion exitosa"})

        return json.dumps({"status": "fail", "message": "insercion no exitosa"})