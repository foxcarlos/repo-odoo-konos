# -*- coding: utf-8 -*-

from odoo import models, fields


class InvasStorageSeq(models.Model):
    _name = "invas.storage.seq"
    _description = "Invas Storage Sequence"
    _order = 'sequence'

    name = fields.Char(
        help="Name of the storage sequence.")
    description = fields.Char(
        help="Description of the storage sequence.")
    sequence = fields.Integer(
        help="Used to reorganize the records.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with this storage sequence.")
