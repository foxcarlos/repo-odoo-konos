# -*- coding: utf-8 -*-

from odoo import models, fields


class InvasRoute(models.Model):
    _name = "invas.route"
    _description = "Invas Route"
    _order = 'sequence'

    name = fields.Char(
        help="Name of the route.")
    description = fields.Char(
        help="Description of the route.")
    sequence = fields.Integer(
        help="Used to reorganize the records.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with this route.")
