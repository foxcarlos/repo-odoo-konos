# -*- coding: utf-8 -*-

from odoo import models, fields


class InvasOutOrderType(models.Model):
    _name = "invas.out.order.type"
    _description = "Invas Order Type"
    _order = 'sequence'

    name = fields.Char(
        help="Name of the type.")
    description = fields.Char(
        help="Description of the type.")
    sequence = fields.Integer(
        help="Used to reorganize the records.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the type.")
