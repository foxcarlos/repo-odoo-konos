# -*- coding: utf-8 -*-

from odoo import api, models, fields, _


class ProductProduct(models.Model):
    _name = "product.product"
    _inherit = ["product.product", "invas.helpers"]

    @api.model
    def _default_inventory_type_id(self):
        company = self.env.user.company_id.id
        domain = [('company_id', '=', company)]
        type_id = self.env['invas.inventory.type'].search(domain, limit=1)
        return type_id or False

    @api.model
    def _default_storage_seq_id(self):
        company = self.env.user.company_id.id
        domain = [('company_id', '=', company)]
        seq_id = self.env['invas.storage.seq'].search(domain, limit=1)
        return seq_id or False

    inactive = fields.Boolean(
        help="Indicates if the product will be created as inactive. "
        "False by default.")
    inventory = fields.Boolean(
        help="Indicates if the product is not included in the inventory when "
        "created. False by default.")
    inventory_type_id = fields.Many2one(
        'invas.inventory.type', string='Inventory Type',
        default=_default_inventory_type_id,
        help="Defines how the product should be treated.")
    storage_seq_id = fields.Many2one(
        'invas.storage.seq', string='Storage Sequence',
        default=_default_storage_seq_id,
        help="Defines how the product should be managed in the warehouse.")
    auto_create = fields.Boolean(
        default=True,
        help="Indicates if the product should be created automatically. "
        "True by default.")
    carrier_track = fields.Boolean(
        help="Indicates if the product is tracked. False by default.")
    user_track = fields.Boolean(
        help="Indicates if the product is tracked. False by default.")
    apply_due_date = fields.Boolean(
        help="This should be enabled only for human consumption products "
        "such as teas, chocolates, etc. False by default.")
    exist = fields.Boolean(
        string="Already exist?", copy=False,
        help="Indicates whether the product exists in INVAS or not.")

    def exist_product(self):
        """Searches for a product in INVAS.

        Returns:
            True if the product already exists, otherwise, False.
        """
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "codigoSku": self.default_code,
        }
        response = instance.send_request(self.name, data, 'search_pt')
        if not response:
            return False

        if not self.exist:
            self.write({'exist': True})

        return True

    def upload_pattern(self):
        """Create a pattern associted to the product."""
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "patron": {
                "codigo": self.default_code,
                "nombre": self.name,
                "descripcion": self.name,
                "patron": self.barcode,
            }
        }
        response = instance.send_request(self.name, data, 'upload_pr')
        if response:
            self.message_post(
                body=_("Product pattern successfully created."))

    def upload_product(self):
        """Creates or updates a product in INVAS."""
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "sku": {
                "codigo": self.default_code,
                "nombre": self.name,
                "descripcion": self.name,
                "operativo": int(self.inactive),
                "inventariar": int(self.inventory),
                "tipoManejoInventario": self.inventory_type_id.name,
                "creacionAut": int(self.auto_create),
                "trackingTransporte": int(self.carrier_track),
                "trackingUsuario": int(self.user_track),
                "manejaFechaVencimiento": int(self.apply_due_date),
                "seqAlmacenamiento": self.storage_seq_id.name,
                "unidad": (self.uom_id.name).upper(),
            }
        }

        categories = self.categ_id.display_name.replace(" ", "").split('/')
        if len(categories) > 3:
            data['sku'].update({'jerarquia1': categories[0]})
        else:
            for count, category in enumerate(categories, 1):
                key = 'jerarquia%s' % (count)
                data['sku'].update({key: category})

        operation = self._context.get('product')
        response = instance.send_request(self.name, data, operation)
        if response:
            if operation == 'upload_pt':
                self.write({'exist': True})
                self.upload_pattern()
            self.message_post(
                body=_("Product successfully created/updated in Invas."))
