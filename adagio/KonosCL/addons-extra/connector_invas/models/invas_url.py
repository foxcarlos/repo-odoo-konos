# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.exceptions import UserError


# IMPORTANT: changes on this list will affect the invas.log.messages model too
OPERATION_TYPES = [
    ('user_token', 'Get Token'),
    ('search_sp', 'Search Supplier'),
    ('upload_sp', 'Create Supplier'),
    ('update_sp', 'Update Supplier'),
    ('search_cm', 'Search Customer'),
    ('upload_cm', 'Create Customer'),
    ('update_cm', 'Update Customer'),
    ('search_pt', 'Search Product'),
    ('upload_pt', 'Create Product'),
    ('update_pt', 'Update Product'),
    ('upload_po', 'Create ASN'),
    ('status_po', 'Check ASN'),
    ('upload_so', 'Create Out Order'),
    ('status_so', 'Check Out Order'),
    ('upload_pr', 'Create pattern'),
]


class InvasUrl(models.Model):
    _name = "invas.url"
    _description = "Invas API URLs"

    name = fields.Char(
        help="Name of the URL.")
    description = fields.Char(
        help="Description of the URL.")
    request_url = fields.Char(
        string="Request URL",
        help="URL to which the request is made.")
    auth = fields.Boolean(
        string="Need Authentication?",
        help="Indicates if the token need to be inclueded in the request "
        "headers. This is necessary for all cases except at the request "
        "of a new token.")
    headers = fields.Char(
        compute='_compute_headers',
        help="Headers to be send in the request.")
    operation = fields.Selection(
        OPERATION_TYPES,
        help="Type of operation associated with the URL.")
    instance_id = fields.Many2one(
        'invas.instance', string="Instance", ondelete='cascade',
        help="Instance associted to the URL.")

    @api.multi
    @api.depends('auth', 'instance_id.token')
    def _compute_headers(self):
        """Computes the headers to be sent in the request."""
        headers = {"Content-Type": "application/json"}
        for record in self:
            if record.auth:
                headers.update({
                    "Authorization": "Bearer %s" % record.instance_id.token})
            record.headers = headers
