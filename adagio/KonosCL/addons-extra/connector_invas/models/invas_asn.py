# -*- coding: utf-8 -*-

from odoo import api, models, fields


class InvasAsnType(models.Model):
    _name = "invas.asn.type"
    _description = "Invas ASN Type"

    name = fields.Char(
        help="Code assigned to the type.")
    description = fields.Char(
        help="Description of the type.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the type.")


class InvasAsn(models.Model):
    _name = "invas.asn"
    _description = "Invas ASN"

    source = fields.Char(
        help="Source of the ASN.")
    name = fields.Char(
        help="The Id of the transaction.")
    number = fields.Char(
        help="The Id of the ASN.")
    transfer_order = fields.Char(
        help="Number of the transfer order.")
    processed = fields.Boolean(
        help="Indicates if this record has already been processed.")
    picking_id = fields.Many2one(
        'stock.picking', string="Related Picking",
        compute='_compute_related_picking',
        help="Picking generated from the original transfer.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the ASN.")
    asn_line = fields.One2many(
        'invas.asn.line', 'asn_id', string='ASN Lines',
        help="ASN lines.")

    @api.multi
    def _compute_related_picking(self):
        """Compute the related picking for the original transfer"""
        for record in self:
            picking_id = self.env['stock.picking'].search([
                ('backorder_id', '=', False),
                ('state', 'in', ['assigned', 'done']),
                ('origin', '=', record.transfer_order),
                ('company_id', '=', record.company_id.id)])
            record.picking_id = picking_id or False

    @api.multi
    def action_process_asn(self):
        """Mark as done the picking in assigned state of a completed ASN"""
        for record in self:
            if record.picking_id and record.picking_id.state != 'done':
                products = {}
                for line in record.asn_line:
                    if line.sku not in products:
                        products.update({line.sku: int(line.quantity)})
                    else:
                        products.update({
                            line.sku: products.get(line.sku) + int(line.quantity)})
                for product, quantity in products.items():
                    move = record.picking_id.move_ids_without_package.filtered(
                        lambda r: r.product_id.default_code == product)
                    move.write({'quantity_done': quantity})
                record.picking_id.action_done()
                record.write({'processed': True})


class InvasAsnLine(models.Model):
    _name = "invas.asn.line"
    _description = "Invas ASN Line"

    lpn = fields.Char(
        help="LPN associated.")
    sku = fields.Char(
        help="SKU associated.")
    quantity = fields.Char(
        help="Quantity received.")
    asn_id = fields.Many2one(
        'invas.asn', string='ASN Reference', ondelete='cascade')
