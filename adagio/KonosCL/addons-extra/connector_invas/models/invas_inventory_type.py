# -*- coding: utf-8 -*-

from odoo import models, fields


class InvasInventoryType(models.Model):
    _name = "invas.inventory.type"
    _description = "Invas Inventory Type"
    _order = 'sequence'

    name = fields.Char(
        help="Code assigned to the type.")
    description = fields.Char(
        help="Description of the type.")
    sequence = fields.Integer(
        help="Used to reorganize the records.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the type.")
