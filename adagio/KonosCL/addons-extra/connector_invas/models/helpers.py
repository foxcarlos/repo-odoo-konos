# -*- coding: utf-8 -*-

from odoo import api, models
from odoo.exceptions import ValidationError


class InvasHelpers(models.AbstractModel):
    _name = "invas.helpers"
    _description = "Common features that can be inherited by regular models"

    _instance = None

    @api.model
    def _invas_instance(self, company_id):
        """Search for a confirmed INVAS instance.
        Args:
            company_id: Current company of the user.
        Returns:
            An invas.instance if it's available, otherwise, raise an exeption.
        """
        if self._instance is None:
            self._instance = self.env['invas.instance'].search([
                ('company_id', '=', company_id),
                ('state', '=', 'confirmed'),
                ('active', '=', True),
            ])
            if not self._instance:
                raise ValidationError('No available INVAS instance found.')
        return self._instance

# REVIEW: creo que el if instance no sirve en este caso
