# -*- coding: utf-8 -*-

from odoo import models, fields


class InvasLocation(models.Model):
    _name = "invas.location"
    _description = "Invas Location"
    _order = 'sequence'

    name = fields.Char(
        help="Name of the location")
    description = fields.Char(
        help="Description of the location.")
    sequence = fields.Integer(
        help="Used to reorganize the records.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the location.")
