# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


# State from INVAS where the purchase order in Odoo can be processed.
STATE = 'RECEPCION COMPLETADA'


class PurchaseOrder(models.Model):
    _name = "purchase.order"
    _inherit = ["purchase.order", "invas.helpers"]

    order_origin_id = fields.Many2one(
        'invas.location', string='Origin',
        help="The location where the purchase order arrives.")
    asn_type_id = fields.Many2one(
        'invas.asn.type', string='ASN Type',
        help="Type of ASN to be created.")
    order_status = fields.Selection([
        ('draft', 'Draft'),
        ('uploaded', 'Uploaded'),
        ('prepared', 'Prepared'),
        ('processed', 'Processed'),
    ], default="draft", copy=False, track_visibility="onchange",
        help="The following values are available:\n"
             "- Draft: Ready to be created into INVAS.\n"
             "- Uploaded: Successfully created into INVAS.\n"
             "- Prepared: Enabled to be processed in Odoo.\n"
             "- Processed: Stock operation validated.")
    description = fields.Text(
        help="Description of the purchase order.")

    def action_upload_po(self):
        """Creates the purchase order as a new ASN into INVAS."""
        self.ensure_one()
        if (self.state != 'purchase' or self.state == 'purchase'
                and self.order_status != 'draft'):
            raise ValidationError(_("You cannot upload this order yet."))

        instance = self._invas_instance(self.env.user.company_id.id)

        if not self.partner_id.is_supplier():
            self.partner_id.with_context({
                'supplier': 'upload_sp'}).upload_supplier()

        for product in self.order_line.mapped('product_id'):
            if not product.exist_product():
                product.with_context({
                    'product': 'upload_pt'}).upload_product()

        data = {
            "asn": {
                "proveedor": self.partner_id.document_number,
                "sitio": self.order_origin_id.name,
                "idasn": self.name,
                "numdoc": self.name,
                "tipoAsn": self.asn_type_id.name,
                "ciego": "0",
                "listaLpnCaja": [dict(
                    idLpn=self.name,
                    sku=line.product_id.default_code,
                    undEnv=int(line.product_uom_qty),
                ) for line in self.order_line],
            }
        }
        response = instance.send_request(self.name, data, 'upload_po')
        if response:
            self.write({'order_status': 'uploaded'})
            self.message_post(
                body=_("%s successfully created in Invas.") % (self.name))

    def get_order_info(self):
        """Gets all the information about the related ASN."""
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "sitio": self.order_origin_id.name,
            "fechaInicio": fields.Date.to_string(self.date_approve),
            "fechaFin": fields.Date.to_string(self.date_approve),
            "tipoAsn": self.asn_type_id.name,
            "listaASN": [{
                'idAsn': self.name,
            }],
        }
        response = instance.send_request(self.name, data, 'status_po')
        return response

    def action_status_po(self):
        """Check the status of the related ASN."""
        self.ensure_one()
        if self.order_status != 'uploaded':
            raise ValidationError(_(
                "You can check the status of uploaded orders only."))

        response = self.get_order_info()
        if response and response.get('listaAsn')[0].get('estado') == STATE:
            self.write({'order_status': 'prepared'})

    @api.multi
    def action_process_po(self):
        """Validates the receipt automatically."""
        self.ensure_one()
        if self.order_status != 'prepared':
            raise ValidationError(_(
                "You can process prepared orders only."))

        response = self.get_order_info()
        if response:
            invas_qty = response.get('listaAsn')[0].get('unrecibidas')
            odoo_qty = sum(self.picking_ids.mapped(
                'move_ids_without_package.product_uom_qty'))

            # REVIEW: what happen when invas_qty != odoo_qty
            if invas_qty == odoo_qty:
                for picking in self.picking_ids:
                    for move in picking.move_ids_without_package:
                        move.write({
                            'quantity_done': move.product_uom_qty,
                        })
                    picking.button_validate()
                self.write({'order_status': 'processed'})
                self.message_post(
                    body=_("Purchase Order successfully procesed."))
