# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


class StockPicking(models.Model):
    _name = "stock.picking"
    _inherit = ["stock.picking", "invas.helpers"]

    order_status = fields.Selection([
        ('draft', 'Draft'),
        ('uploaded', 'Uploaded'),
    ], default="draft", copy=False, track_visibility="onchange",
        help="The following values are available:\n"
             "- Draft: Ready to be created into INVAS.\n"
             "- Uploaded: Successfully created into INVAS.")
    order_origin_id = fields.Many2one(
        'invas.location', string='Origin',
        help="The location from where the out order will leave.")
    order_dest_id = fields.Many2one(
        'invas.location', string='Destination',
        help="Destination of the out order, usually some store, production "
        "area or sale channel.")
    out_order_type_id = fields.Many2one(
        'invas.out.order.type', string='Out Order Type',
        help="Type of the out order.")
    out_order_route_id = fields.Many2one(
        'invas.route', string='Out Order Route',
        help="Route of the out order.")

    def check_availability(self, location, type_code):
        """Check availability of a location"""
        # if not location or type_code != 'internal':
        #     return False
        print(location.id, type_code)
        domain = [
            ('state', '=', 'assigned'),
            ('picking_type_code', '=', type_code),
            ('location_id', '=', location.id),
        ]
        picking_id = self.env['stock.picking'].search(domain)
        return picking_id or False

    def clone_virtual_location(self):
        """Allows to clone a specific virtual location and assign it to the
        current picking"""
        usage = 'transit'
        parent = self.env.ref('stock.stock_location_locations_virtual')
        if not self.location_dest_id or\
                self.location_dest_id.usage != usage or\
                self.location_dest_id.location_id.id != parent.id:
            raise ValidationError(_(
                "A valid transit location type must be selected"))

        # Rule to be cloned
        rule = self.env['stock.rule'].search([
            ('location_src_id', '=', self.location_dest_id.id),
            ('action', '=', 'push')])

        if not rule:
            raise ValidationError(_(
                "There's no rule that can be cloned for this location"))

        # Check is there's an available location
        rules = self.env['stock.rule'].search([
            ('location_id', '=', rule.location_id.id),
            ('action', '=', 'push')])

        available = [r.location_src_id.complete_name for r in rules
                     if not self.check_availability(r.location_src_id,
                                                    r.picking_type_id.code)]

        if available:
            raise ValidationError(_(
                "It's not necessary to clone a location, the following are "
                "available:\n %s") % ('\n'.join(available)))

        # Create a new Virtual Location
        number = self.env['stock.location'].search_count([
            ('usage', '=', usage)])
        location_vals = {
            'name': '%s (%s)' % (self.location_dest_id.name, str(number + 1)),
        }
        location = self.location_dest_id.copy(default=location_vals)

        # Create a new rule for the location
        rule_vals = {
            'name': '%s (%s)' % (rule.name, str(number + 1)),
            'location_src_id': location.id,
        }
        rule.copy(default=rule_vals)
        self.write({'location_dest_id': location.id})

    @api.multi
    def action_confirm(self):
        """Hack to avoid group picking, due to the ASN verification process,
        pickings need to be processed one at a time"""
        picking = self.check_availability(
            self.location_dest_id, self.picking_type_code)

        print("****ENTRO AL ORIGINAL***")
        if self.partner_id.name == 'TEA GROUP SPA':
            picking = None

        if picking:
            raise ValidationError(_(
                "There is currently a pending operation (%s) for %s"
                ) % (picking.name, self.location_dest_id.complete_name))
        return super(StockPicking, self).action_confirm()

    def button_validate(self):
        """Ensures that the data related to Invas is provided when necessary"""
        values = [
            self.order_origin_id, self.order_dest_id,
            self.out_order_type_id, self.out_order_route_id
        ]
        if self.picking_type_code == 'internal' and not all(values):
            raise ValidationError(_(
                "All the fields within the invas tab must be provided"))
        return super(StockPicking, self).button_validate()

    def action_upload_pick(self):
        """Creates a new out order into INVAS."""
        self.ensure_one()
        if (self.state != 'done' or self.state == 'done'
                and self.order_status != 'draft'):
            raise ValidationError(_("You cannot upload this picking yet"))

        instance = self._invas_instance(self.env.user.company_id.id)

        if not self.partner_id.is_customer():
            self.partner_id.with_context({
                'customer': 'upload_cm'}).upload_customer()

        for product in self.move_ids_without_package.mapped('product_id'):
            if not product.exist_product():
                product.with_context({
                    'product': 'upload_pt'}).upload_product()

        data = {
            "os": {
                "idOs": self.name,
                "cliente": self.partner_id.document_number,
                "fechaPactada": fields.Date.to_string(
                    self.scheduled_date).replace('-', '/'),
                "sitio": self.order_origin_id.name,
                "tipoOs": self.out_order_type_id.name,
                "destino": self.out_order_route_id.name,
                "sitioDestino": self.order_dest_id.name,
                "asignParcial": 0,
                "listaTipoInsumo": [dict(
                    producto=line.product_id.default_code,
                    unidadesOrdenadas=int(line.product_uom_qty),
                ) for line in self.move_ids_without_package],
            }
        }

        response = instance.send_request(self.name, data, 'upload_so')
        if response:
            self.write({'order_status': 'uploaded'})
            self.message_post(
                body=_("%s successfully created in Invas.") % (self.name))
