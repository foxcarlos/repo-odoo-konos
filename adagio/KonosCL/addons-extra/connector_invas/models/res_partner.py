# -*- coding: utf-8 -*-

from odoo import api, models, fields, _


class ResPartner(models.Model):
    _name = "res.partner"
    _inherit = ["res.partner", "invas.helpers"]

    exist_supplier = fields.Boolean(
        string="INVAS supplier?",
        help="Indicates whether the partner already exists as supplier "
        "in INVAS or not.")
    exist_customer = fields.Boolean(
        string="INVAS customer?",
        help="Indicates whether the partner already exists as customer "
        "in INVAS or not.")

    def is_supplier(self):
        """Verifies if the supplier already exists in INVAS.

        Returns:
            True if the supplier exists, otherwise, False.
        """
        # REVIEW: el RUT se formatea con . automaticamente
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "findproveedor": self.document_number,
        }
        response = instance.send_request(self.name, data, 'search_sp')
        if not response:
            return False

        if not self.exist_supplier:
            self.write({'exist_supplier': True})

        return True

    def upload_supplier(self):
        """Creates or updates a new supplier into INVAS."""
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "proveedor": {
                "codigo": self.document_number,
                "nombre": self.name,
                "descripcion": self.comment,
            }
        }
        operation = self._context.get('supplier')
        response = instance.send_request(self.name, data, operation)
        if response:
            self.write({'exist_supplier': True})
            self.message_post(body=_(
                "Supplier successfully created/updated in Invas."))

    def is_customer(self):
        """Verifies if the customer already exists in INVAS.

        Returns:
            True if the customer exists, otherwise, False.
        """
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "findcliente": self.document_number,
        }
        response = instance.send_request(self.name, data, 'search_cm')
        if not response:
            return False

        if not self.exist_customer:
            self.write({'exist_customer': True})

        return True

    def upload_customer(self):
        """Creates or updates a customer into INVAS."""
        self.ensure_one()
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "cliente": {
                "codigo": self.document_number,
                "nombre": self.name,
                "direccion": self.street,
                "telefonocliente": self.phone,
                "mailcliente": self.email,
                "clienteciudad": self.city,
                "clientepais": self.country_id.name,
            }
        }
        operation = self._context.get('customer')
        response = instance.send_request(self.name, data, operation)
        if response:
            self.write({'exist_customer': True})
            self.message_post(
                body=_("Customer successfully created/updated in Invas."))
