# -*- coding: utf-8 -*-

from odoo import models, fields
from .invas_url import OPERATION_TYPES


class InvasLogMessages(models.Model):
    _name = "invas.log.messages"
    _description = "Invas Log Messages"

    instance_id = fields.Many2one(
        "invas.instance", string="Instance",
        help="Instance associated with the log message.")
    operation = fields.Selection(
        OPERATION_TYPES,
        help="Type of operation that generated the message.")
    message = fields.Text(
        help="Message content. E.g. Invas API request failed.")
