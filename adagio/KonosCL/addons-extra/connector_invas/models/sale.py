# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


# State from INVAS where the sale order in Odoo can be processed.
STATE = 'DESPACHADO'


class SaleOrder(models.Model):
    _name = "sale.order"
    _inherit = ["sale.order", "invas.helpers"]

    @api.model
    def _default_origin_id(self):
        company = self.env.user.company_id.id
        origin_ids = self.env['invas.location'].search([
            ('company_id', '=', company)], limit=1)
        return origin_ids or False

    @api.model
    def _default_out_order_type_id(self):
        company = self.env.user.company_id.id
        domain = [('company_id', '=', company)]
        type_id = self.env['invas.out.order.type'].search(domain, limit=1)
        return type_id or False

    @api.model
    def _default_out_order_route_id(self):
        company = self.env.user.company_id.id
        origin_ids = self.env['invas.route'].search([
            ('company_id', '=', company)], limit=1)
        return origin_ids or False

    order_status = fields.Selection([
        ('draft', 'Draft'),
        ('uploaded', 'Uploaded'),
        ('prepared', 'Prepared'),
        ('processed', 'Processed'),
    ], default="draft", copy=False, track_visibility="onchange",
        help="The following values are available:\n"
             "- Draft: Ready to be created into INVAS.\n"
             "- Uploaded: Successfully created into INVAS.\n"
             "- Prepared: Enabled to be processed in Odoo.\n"
             "- Processed: Stock operation validated.")
    order_origin_id = fields.Many2one(
        'invas.location', string='Origin', default=_default_origin_id,
        help="The location from where the out order will leave.")
    order_dest_id = fields.Many2one(
        'invas.location', string='Destination',
        help="Destination of the out order, usually some store, production "
        "area or sale channel.")
    out_order_type_id = fields.Many2one(
        'invas.out.order.type', string='Out Order Type',
        default=_default_out_order_type_id,
        help="Type of the out order.")
    out_order_route_id = fields.Many2one(
        'invas.route', string='Out Order Route',
        default=_default_out_order_route_id,
        help="Route of the out order.")
    assignment = fields.Boolean(
        help="Enable this feature for a better distribution of the order.")

    def action_upload_so(self):
        """Creates a new out order into INVAS."""
        self.ensure_one()
        if (self.state != 'sale' or self.state == 'sale'
                and self.order_status != 'draft'):
            raise ValidationError(_("You cannot upload this order yet."))

        instance = self._invas_instance(self.env.user.company_id.id)

        if not self.partner_id.is_customer():
            self.partner_id.with_context({
                'customer': 'upload_cm'}).upload_customer()

        for product in self.order_line.filtered(
                lambda r: r.product_id.type == 'product').mapped('product_id'):
            if not product.exist_product():
                product.with_context({
                    'product': 'upload_pt'}).upload_product()

        data = {
            "os": {
                "idOs": self.name,
                "cliente": self.partner_id.document_number,
                "fechaPactada": fields.Date.to_string(
                    self.confirmation_date).replace('-', '/'),
                "sitio": self.order_origin_id.name,
                "tipoOs": self.out_order_type_id.name,
                "destino": self.out_order_route_id.name,
                "sitioDestino": self.order_dest_id.name,
                "asignParcial": int(self.assignment),
                "listaTipoInsumo": [dict(
                    producto=line.product_id.default_code,
                    unidadesOrdenadas=int(line.product_uom_qty),
                ) for line in self.order_line if line.product_id.type == 'product'],
            }
        }
        response = instance.send_request(self.name, data, 'upload_so')
        if response:
            self.write({'order_status': 'uploaded'})
            self.message_post(
                body=_("%s successfully created in Invas.") % (self.name))

    def get_order_info(self):
        """Gets all the information about the related out order."""
        instance = self._invas_instance(self.env.user.company_id.id)
        data = {
            "listaOS": [{
                'idOs': self.name,
            }],
        }
        response = instance.send_request(self.name, data, 'status_so')
        return response

    def action_status_so(self):
        """Check the status of the related out order."""
        self.ensure_one()
        if self.order_status != 'uploaded':
            raise ValidationError(_(
                "You can check the status of uploaded orders only."))

        response = self.get_order_info()
        if response and response.get('listaOs')[0].get('estado') == STATE:
            self.write({'order_status': 'prepared'})

    @api.multi
    def action_process_so(self):
        """Validate the delivery automatically."""
        self.ensure_one()
        if self.order_status != 'prepared':
            raise ValidationError(_(
                "You can process prepared orders only."))

        for picking in self.picking_ids:
            if picking.state == 'assigned':
                for move in picking.move_ids_without_package:
                    move.write({
                        'quantity_done': move.product_uom_qty,
                    })
                picking.button_validate()
        self.write({'order_status': 'processed'})
        self.message_post(
            body=_("Sale Order successfully procesed."))
