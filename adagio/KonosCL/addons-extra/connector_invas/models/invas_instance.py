# -*- coding: utf-8 -*-

import json
import requests
from ast import literal_eval
from odoo import models, fields, _
from odoo.exceptions import Warning


class InvasInstance(models.Model):
    _name = "invas.instance"
    _description = "Invas Instance"

    active = fields.Boolean(
        help="Allows to hide the instance without removing it.")
    name = fields.Char(
        required=True, help="Name of the instance.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        help="Company associated with the instance.")
    request_url_ids = fields.One2many(
        'invas.url', 'instance_id', string="URLs",
        help="Available URLs for the instance.")
    user = fields.Char(
        required=True, copy=False, help="A valid INVAS user.")
    password = fields.Char(
        required=True, copy=False, help="Password for the INVAS user.")
    token = fields.Char(
        copy=False,
        help="Token to allow sending authenticated requests to the INVAS API. "
        "As a security policy, it's valid for (1) day only.")
    token_date = fields.Datetime(
        help="The date and time the last token was generated.")
    state = fields.Selection([
        ('not_confirmed', 'Not Confirmed'),
        ('confirmed', 'Confirmed'),
        ], default='not_confirmed', copy=False, help="State of the instance.")

    def send_request(self, name, data, operation):
        """Send a POST request to the INVAS API.

        Args:
            name: Identifies who generates the request. E.g. John Doe / PO00007
            data: Information to send in the request.
            operation: Type of operation to be performed.
        Returns:
            A dict with the data resulting from the request if it was completed
            successfully, otherwise, an empty dict.
        """
        content = {}
        message = ""

        # at least a valid user and password must be provided
        data.update({
            'usuario': self.user,
            'password': self.password,
        })

        url_id = self.request_url_ids.filtered(
            lambda r: r.operation == operation)
        try:
            response = requests.post(
                url=url_id.request_url, headers=literal_eval(url_id.headers),
                data=json.dumps(data))
            content = response.json()
            if content.get('error') or content.get('Error'):
                msg = content.get('mensaje') or content.get('Mensaje')
                message = _("[%s] request failed with message: %s") % (
                    name, msg)
                content = {}
        except Exception as e:
            message = _("[%s] Request returned an error: '%s'") % (
                name, str(e))

        if message:
            self.env['invas.log.messages'].create({
                'instance_id': self.id,
                'operation': operation,
                'message': message,
            })

        return content

    def action_get_token(self):
        """Obtain a valid token from INVAS."""
        response = self.send_request(self.env.user.name, {}, 'user_token')
        if response:
            self.write({
                'token': response.get('token'),
                'token_date': fields.Datetime.now(),
                'state': 'confirmed',
            })
