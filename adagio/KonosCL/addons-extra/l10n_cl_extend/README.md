# Extension for Chilean Localization

Enables/disables the extra features added by the chilean localization.

This is necessary because some of the added features should apply only
on chilean companies.

### Examples

- Companies.

![Image 01](static/description/company_01.png)

![Image 02](static/description/company_02.png)

- Contacts

![Image 03](static/description/contact_01.png)

![Image 04](static/description/contact_02.png)

- Journals

![Image 05](static/description/journal_01.png)

![Image 06](static/description/journal_02.png)

- Invoices

![Image 07](static/description/invoice_01.png)

![Image 08](static/description/invoice_02.png)
