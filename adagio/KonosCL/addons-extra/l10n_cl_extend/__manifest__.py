# -*- coding: utf-8 -*-
{
    'name': "Extension for Chilean Localization",
    'summary': """
        Enables/disables the extra features added by the chilean localization.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Localization/Chile',
    'version': '12.0.0.0.0',

    'depends': [
        'l10n_cl_fe',
        'l10n_cl_stock_picking',
        'l10n_cl_partner',
    ],

    'data': [
        'security/extend_security.xml',
        'views/account_invoice_view.xml',
        'views/account_view.xml',
        'views/res_company.xml',
        'views/res_partner.xml',
        'views/sale_views.xml',
        'views/account_tax_views.xml',
        'views/stock_views.xml',
    ],

    'demo': [
        'demo/res_company_demo.xml',
        'demo/res_users_demo.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
