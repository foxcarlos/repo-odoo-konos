# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AccountJournal(models.Model):
    _name = 'account.journal'
    _inherit = ['account.journal', 'helper.class']

    l10n_cl_active = fields.Boolean(
        compute='_compute_l10n_cl_active', string="l10n cl",
        default=lambda self: self._enable_l10n_cl_features(),
        help="Indicates if the chilean features should be enabled or not."
    )
    use_documents = fields.Boolean(
        default=False,
    )

    @api.multi
    def _compute_l10n_cl_active(self):
        """Computes the value for the l10n_cl_active field"""
        for record in self:
            record.l10n_cl_active = self._enable_l10n_cl_features()

    @api.onchange('type')
    def _onchange_type(self):
        """Use documents fields should be required only if the company is from
        Chile and the journal type is sale or purchase"""
        if self.l10n_cl_active and self.type in ['sale', 'purchase']:
            self.use_documents = True
        else:
            self.use_documents = False
