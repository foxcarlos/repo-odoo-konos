# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ResPartner(models.Model):
    _name = "res.partner"
    _inherit = ['res.partner', 'helper.class']

    l10n_cl_active = fields.Boolean(
        compute='_compute_l10n_cl_active', string="l10n cl",
        default=lambda self: self._enable_l10n_cl_features(),
        help="Indicates if the chilean features should be enabled or not."
    )

    @api.multi
    def _compute_l10n_cl_active(self):
        """Computes the value for the l10n_cl_active field"""
        for record in self:
            record.l10n_cl_active = self._enable_l10n_cl_features()
