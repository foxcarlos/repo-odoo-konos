# -*- coding: utf-8 -*-

from odoo import api, fields, models


class StockPicking(models.Model):
    _name = "stock.picking"
    _inherit = ['stock.picking', 'helper.class']

    l10n_cl_active = fields.Boolean(
        compute='_compute_l10n_cl_active', string="l10n cl",
        default=lambda self: self._enable_l10n_cl_features(),
        help="Indicates if the chilean features should be enabled or not."
    )

    @api.multi
    def _compute_l10n_cl_active(self):
        """Computes the value for the l10n_cl_active field"""
        for record in self:
            record.l10n_cl_active = self._enable_l10n_cl_features()

    @api.onchange('picking_type_id')
    def onchange_picking_type(self):
        if self.l10n_cl_active and self.picking_type_id:
            self.use_documents = self.picking_type_id.code not in ["incoming"]
        else:
            self.use_documents = False

    def set_use_document(self):
        if not self.l10n_cl_active:
            return False
        return (
            self.picking_type_id and self.picking_type_id.code != 'incoming')


class StockLocation(models.Model):
    _name = "stock.location"
    _inherit = ['stock.location', 'helper.class']

    l10n_cl_active = fields.Boolean(
        compute='_compute_l10n_cl_active', string="l10n cl",
        default=lambda self: self._enable_l10n_cl_features(),
        help="Indicates if the chilean features should be enabled or not."
    )

    @api.multi
    def _compute_l10n_cl_active(self):
        """Computes the value for the l10n_cl_active field"""
        for record in self:
            record.l10n_cl_active = self._enable_l10n_cl_features()
