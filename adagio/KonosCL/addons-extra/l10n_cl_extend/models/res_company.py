# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ResCompany(models.Model):
    _name = "res.company"
    _inherit = ['res.company', 'helper.class']

    l10n_cl_active = fields.Boolean(
        compute='_compute_l10n_cl_active', string="l10n cl",
        default=lambda self: self._enable_l10n_cl_features(),
        help="Indicates if the chilean features should be enabled or not."
    )
    document_number = fields.Char(
        required=False,
    )
    document_type_id = fields.Many2one(
        required=False,
    )

    @api.multi
    def _compute_l10n_cl_active(self):
        """Computes the value for the l10n_cl_active field"""
        for record in self:
            record.l10n_cl_active = self._enable_l10n_cl_features()
