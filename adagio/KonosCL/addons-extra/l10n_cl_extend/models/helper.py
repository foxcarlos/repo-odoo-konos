# -*- coding: utf-8 -*-

from odoo import api, models


class HelperClass(models.AbstractModel):
    _name = 'helper.class'
    _description = 'Features that can then be inherited by regular models'

    @api.model
    def _enable_l10n_cl_features(self):
        """Returns False if the country of the company associated to the
        current user is different from Chile, otherwise, returns True.

        :rtype: bool
        """
        country = self.env.ref('base.cl')
        current = self.env.user.company_id.country_id
        return current == country or False
