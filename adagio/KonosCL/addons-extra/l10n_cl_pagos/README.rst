# Odoo Chile - Tesorería y Pagos Salientes
[l10n_cl_pagos]

## Odoo (ERP/CRM)
 - Odoo 12 Localización chilena
 - Módulos ajustados para versión 12 (Enterprise)

## Scope 
**Features list: Chilean Localization**

    * Órdenes de Pago saliente
    * Pagos masivos para proveedores y empleados
    * Generación de archivo de nómina de pago para bancos (Chile y BCI)
    * Medios y vías de Pagos para Chile<br/>Total a Cobrar y a Pagar en Maestro de Terceros




 ## To do
 
 - Documentation
 
 ##
 
 1. Viene con  Bancos Chile y BCI
 2. No es compatible con Account Payment Group (Banco y Caja)

Modos de Pago y Archivos Planos existentePago remuneraciones BCIREM - Transfer BCI
Pago Proveedores BCI y Banco de ChilePRV - Transfer BCI
PRV - Transfer Banco de Chile
NOTAEl nombre del modo de pago debe ser exacto. El Usuario debe tener permisos de acceso. 
 https://youtu.be/C-XmXFSzW-Y
 
 ## Disclaimer
 
 - Open source software: use at your own risk. 
 - Public private repository. Not currently supporting third parties
