# General Disable Create

In Modules Sales, Purchase, Inventory and Invoicing,
regarding to **create** or **edit** actions on specific fields, users
won't be able to execute them. This customization is meant to be a
preventive measure for future conflicts.

Refer to the following [document](https://drive.google.com/open?id=1iWqhQVIgdraEtbVpXHiSCN_q1mRjjKxl) in
order to know all the fields that were modified.

### Examples

- For **Sales View**

![Image_0](static/description/Image_0.png)
