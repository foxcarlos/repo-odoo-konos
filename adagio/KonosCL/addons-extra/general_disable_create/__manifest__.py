# -*- coding: utf-8 -*-
{
    'name': "General Disable Create",

    'summary': """
        Disables Create and Edit... action for some fields.
    """,

    'author': "Odoolatam",
    'website': "https://odoolatam.odoo.com/",

    'category': 'Uncategorized',
    'version': '12.0.0.0.0',

    'depends': [
        'sale_management',
        'purchase',
        'stock',
        'account',
    ],

    'data': [
        'views/sales_views.xml',
        'views/purchase_views.xml',
        'views/stock_views.xml',
        'views/account_views.xml',
        'views/res_partner_views.xml',
        'views/product_views.xml',
    ],

    'demo': [
    
    ],
}
