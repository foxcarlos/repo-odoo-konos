{
    "name": """Automatic Folio Consumption for Chile\
    """,
    'version': '0.0.1',
    'category': 'Localization/Chile',
    'sequence': 12,
    'author':  'Konos',
    'website': 'https://konos.cl',
    'license': 'AGPL-3',
    'summary': 'Automatic Folio Consumption for Chile',
    'description': """
Automatic Folio Consumption for Chile
""",
    'depends': [
            'l10n_cl_fe',
        ],
    'data': [
            'data/cron.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}