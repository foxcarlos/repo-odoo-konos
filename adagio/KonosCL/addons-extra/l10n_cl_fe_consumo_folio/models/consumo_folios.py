# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging


_logger = logging.getLogger(__name__)



class ConsumoFolios(models.Model):
    _inherit = "account.move.consumo_folios"


    @api.multi
    def _create_folio_consumption(self):
        values = {}
        values.update( {'name':fields.Date.context_today(self),
                        'date': fields.Date.context_today(self),
                        'currency_id': self.env.user.company_id.currency_id.id,
                        'company_id': self.env.user.company_id.id,
                                   })

        folio_consumption = self.create(values)
        folio_consumption.set_data()
        folio_consumption.get_totales()
        folio_consumption._resumenes()
        folio_consumption.validar_consumo_folios()
        folio_consumption.do_dte_send_consumo_folios()


    @api.multi
    def _send_folio_consumption(self):
        for rec in self.filtered(lambda r: r.state in ['NoEnviado','Rechazado']):
            rec.do_dte_send_consumo_folios()

