# -*- coding: utf-8 -*-
{
    "name": """POS Invoice Refund for Chile\
    """,
    'version': '0.0.1',
    'category': 'Localization/Chile',
    'sequence': 12,
    'author':  'Konos',
    'website': 'https://konos.cl',
    'license': 'AGPL-3',
    'summary': '',
    'description': """
Folio Consumption Report for Chile.
""",
    'depends': [
            'l10n_cl_dte_point_of_sale',
],
    'data': [
            'views/l10n_cl_point_of_sale_invoice_refund.xml',
            'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
