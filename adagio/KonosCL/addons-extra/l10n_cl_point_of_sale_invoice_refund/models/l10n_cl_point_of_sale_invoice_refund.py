# -*- coding: utf-8 -*-


from odoo import fields, models, api, _
import logging
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)




class POSInvoiceRefund(models.Model):
    _name = "l10n_cl_point_of_sale_invoice_refund"
    _description = 'POS Invoice Refund'

    account_invoice = fields.Many2one('account.invoice',
                    domain=[
                    ('type', '=', 'out_invoice'),
                    ('sii_result', 'in', ['Proceso','Reparo']),
                    ('state', '=', 'paid')])

    filter_refund = fields.Selection(
            [
                ('1', 'Anula Documento de Referencia'),
                ('2', 'Corrige texto Documento Referencia'),
                ('3', 'Corrige montos'),
            ],
            default='1',
            string='Refund Method',
            required=True,
            help='Refund base on this type. You can not Modify and Cancel if the invoice is already reconciled',
        )
    motivo = fields.Char("Motivo",required=True,)

    company_id = fields.Many2one('res.company', string='Company', required = True, default=lambda self: self.env.user.company_id)
    date = fields.Date(string="Date", required=True,default=lambda self: fields.Date.context_today(self),)

    @api.onchange('filter_refund')
    def _onchange_filter_refund(self):
        if self.filter_refund == '2':
            self.motivo = "Debe Decir: "
        elif self.filter_refund == '1':
            self.motivo = "Devolución"
        else:
            self.motivo = "Corrige Montos"

    #Here will Create DTE, Envelope, Attachment and Post Message. This is The Heart
    def create_refund(self):
        clone_list = []
        context = dict(self._context or {})

        current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)], limit=1)
        if not current_session:
            raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

        journal_document_class_id = self.env['account.journal.sii_document_class'].search(
            [('journal_id.type','=', 'sale'), ('sequence_id.dte_caf_ids','!=', False),
             ('sii_document_class_id.sii_code', 'in', [61])], limit=1)
        

            
        PosOrder = self.env['pos.order']
        PosOrder_line = self.env["pos.order.line"]


        amount = 0
        payments = self.account_invoice._get_payments_vals()
        for p in payments:
            amount += p['amount']

        #TODO IN ADAGIO ADD EMPLOYEE AND USER ID
        values = {
            'name': self.account_invoice.name or '' + ' REFUND', # not used, name forced by create
            'partner_id': self.account_invoice.partner_id.id,
            'user_id': self.env.user.id,
            'session_id': current_session.id,
            'date_order': fields.Datetime.now(),
            'pos_reference': self.account_invoice.name,
            'lines': False,
            'amount_tax': -self.account_invoice.amount_tax,
            'amount_total': -self.account_invoice.amount_total,
            'amount_paid': 0,
            'amount_return': 0,
            'sequence_id': journal_document_class_id.sequence_id.id,
            'document_class_id': journal_document_class_id.sii_document_class_id.id,
            'sii_document_number': 0,
            'signature': False,
            'referencias': [[5,],[0,0, {
            'origen': int(self.account_invoice.sii_document_number),
            'sii_referencia_TpoDocRef': self.account_invoice.journal_document_class_id.sii_document_class_id.id,
            'sii_referencia_CodRef': self.filter_refund,
            'motivo': self.motivo,
            'fecha_documento': self.account_invoice.date_invoice
            }]],
        }
        clone_id = self.env['pos.order'].create(values)

        for line in self.account_invoice.invoice_line_ids:
            clone_line = PosOrder_line.create({
                'name': line.name + _(' REFUND'),
                'order_id': clone_id.id,
                'qty': -line.quantity,
                'price_unit': line.price_unit,
                'product_id':line.product_id.id,
                'tax_ids':[(6, 0, line.invoice_line_tax_ids.ids)],
                'price_subtotal': -line.price_subtotal,
                'price_subtotal_incl': -line.price_subtotal,
                })
        PosOrder += clone_id

        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': PosOrder.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }




class POS(models.Model):
    _inherit = 'pos.order'

    change_taxes = fields.Boolean(
            string='Exempt',
            default=False,
            help="""This lets you change taxes.""")



class POSL(models.Model):
    _inherit = 'pos.order.line'

    #Este método coloca el impuesto en la linea
    @api.depends('order_id', 'order_id.fiscal_position_id','order_id.change_taxes')
    def _get_tax_ids_after_fiscal_position(self):
        for line in self:
            imp = self.env['account.tax'].search([
            ('sii_code', '=', 0),
            ('type_tax_use', '=', 'sale'),
            ('company_id', '=', line.order_id.company_id.id)
        ], limit=1)
            if not line.order_id.change_taxes:
                line.tax_ids_after_fiscal_position = line.order_id.fiscal_position_id.map_tax(line.tax_ids, line.product_id, line.order_id.partner_id)
            else:
                line.tax_ids_after_fiscal_position = line.order_id.fiscal_position_id.map_tax(imp, line.product_id, line.order_id.partner_id)
            line._onchange_qty()



    #Este coloca Subtotal y total
    @api.depends('price_unit', 'tax_ids', 'qty', 'discount', 'product_id')
    def _compute_amount_line_all(self):

        imp = self.env['account.tax'].search([
            ('sii_code', '=', 0),
            ('type_tax_use', '=', 'sale'),
            ('company_id', '=', self.order_id.company_id.id)
        ], limit=1)

        self.ensure_one()
        fpos = self.order_id.fiscal_position_id
        if not self.order_id.change_taxes:
            tax_ids_after_fiscal_position = fpos.map_tax(self.tax_ids, self.product_id, self.order_id.partner_id) if fpos else self.tax_ids
        else:
            tax_ids_after_fiscal_position = fpos.map_tax(imp, self.product_id, self.order_id.partner_id) if fpos else self.tax_ids
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        taxes = tax_ids_after_fiscal_position.compute_all(price, self.order_id.pricelist_id.currency_id, self.qty, product=self.product_id, partner=self.order_id.partner_id)

        if not self.order_id.change_taxes:
            return {
                'price_subtotal_incl': taxes['total_excluded'],
                'price_subtotal': taxes['total_excluded'],
            }
        else:
            return {
                'price_subtotal_incl': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            }


    @api.onchange('qty', 'discount', 'price_unit', 'tax_ids')
    def _onchange_qty(self):
        if self.product_id:
            if not self.order_id.pricelist_id:
                raise UserError(_('You have to select a pricelist in the sale form.'))
            price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
            self.price_subtotal = self.price_subtotal_incl = price * self.qty
            if (self.product_id.taxes_id and not self.order_id.change_taxes):
                taxes = self.product_id.taxes_id.compute_all(price, self.order_id.pricelist_id.currency_id, self.qty, product=self.product_id, partner=False)
                self.price_subtotal = taxes['total_excluded']
                self.price_subtotal_incl = taxes['total_included']

