======================
Chilean Basic Accounting Books
======================

.. contents::
   :local:

Configuracion
=============

*.	Instalar Chilean Landed Cost
*.	Activar Multimoneda (Asegúrate de que Taza de Cambio Automática no esté tildada)
*.	Instalar Financial Indicators (l10n_cl_financial_indicators). Revisar Tarea Programada esté todas las mañanas a las 9:00 am. 
*.	En Facturas de Proveedor nos aseguramos tener Documento (Declaración de Ingreso (DIN))


Uso
===

*

Creditos
========

Contribuidores
--------------

* `Konos <https://www.konos.cl>`_:

  * Nelson Sanchez <nramirez@konos.cl>

Mantenedores
------------

Ese modulo esta mantenido por Konos.



Estan bienvenidos para contribuir. Para aprender como, por favor visita https://odoo-community.org/page/Contribute.
