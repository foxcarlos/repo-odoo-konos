# Copyright (C) 2020 Konos
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "Chilean Stock Landed Costs",
    "version": "0.0.1",
    "license": "LGPL-3",
    "summary": "Chilean Stock Landed Costs",
    "author": "Konos",
    "maintainer": "Konos",
    "website": "https://www.konos.cl",
    "depends": [
        "l10n_cl_fe",
        "stock_landed_costs",
        "l10n_cl_financial_indicators",
        "account_accountant"
    ],
    "data": [
        "data/product.xml",
        "data/account_journal.xml",
        "data/res_partner.xml",
    ],
    "application": False,
    "sequence": 0,
}

