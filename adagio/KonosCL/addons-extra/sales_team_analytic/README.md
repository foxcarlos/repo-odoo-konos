# Sale Order Approval
Send sales orders to an approval stage when the order amount exceeds
the partner's credit limit.


## Table of contents
* [Configuration](#configuration)
* [Usage](#usage)
* [Credits](#credits)
  * [Authors](#authors)
  * [Contributors](#contributors)
  * [Maintainers](#maintainers)


## Configuration
* Enable the permission to users responsible for approving sales orders.

![image_01](static/description/image_01.png)

* Establish the desired credit limit policy for the partner.

![image_02](static/description/image_02.png)

## Usage
* Now, sales orders that exceed the partner credit limit should be approved by
an authorized user.

![image_03](static/description/image_03.gif)


## Credits

### Authors
* Konos Soluciones & Servicios

### Contributors
* Alexander Olivares <<aolivares@konos.cl>>

### Maintainers
This module is maintained by **Konos Soluciones & Servicios**.

Current maintainer:
* Alexander Olivares <<aolivares@konos.cl>>
