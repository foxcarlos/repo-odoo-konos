# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models,fields, api, _


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    analytic_account_id = fields.Many2one(
        'account.analytic.account', 'Analytic Account',
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="The analytic account related to a sales order.")
