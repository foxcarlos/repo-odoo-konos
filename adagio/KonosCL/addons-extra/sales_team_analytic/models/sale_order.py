# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm(self):
        for order in self:
            if order.team_id and order.team_id.analytic_account_id and not order.analytic_account_id:
                order.analytic_account_id = order.team_id.analytic_account_id.id
            return super(SaleOrder, self).action_confirm()