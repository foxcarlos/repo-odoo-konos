# -*- coding: utf-8 -*-
{
    'name': "Sales Team Analytic",

    'summary': """
        Assign Sales Team Analytic Account.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'sale_management',
    ],

    'data': [
        'views/crm_team_views.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
