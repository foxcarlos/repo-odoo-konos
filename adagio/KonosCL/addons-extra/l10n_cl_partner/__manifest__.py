# -*- coding: utf-8 -*-
{
    'name': "Partners for Chile",

    'summary': "Autocompletes partner information using the Konos API",

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Localization/Chile',
    'version': '0.0.0.3',

    'depends': [
        'l10n_cl_fe',
    ],

    'data': [
        "data/ir_config_parameter.xml",
        'data/ir_actions_server.xml',
        'views/res_partner.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
