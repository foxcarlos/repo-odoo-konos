# -*- encoding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _
import re
import logging
import requests
import json
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'


    @api.onchange('document_number', 'document_type_id')
    def onchange_vat(self):
        reponse=False
        mod_obj = self.env['ir.model.data']
        if self.document_number and ((
            'sii.document_type',
            self.document_type_id.id) == mod_obj.get_object_reference(
                'l10n_cl_fe', 'dt_RUT') or ('sii.document_type',
                self.document_type_id.id) == mod_obj.get_object_reference(
                    'l10n_cl_fe', 'dt_RUN')):
            document_number = (
                re.sub('[^1234567890Kk]', '', str(
                    self.document_number))).zfill(9).upper()
            if not self.check_vat_cl(document_number):
                return {'warning': {'title': _('Rut Erróneo'),
                                    'message': _('Rut Erróneo'),
                                    }
                        }

            vat = 'CL%s' % document_number
            exist = self.env['res.partner'].search(
                [
                    ('vat','=', vat),
                    ('vat', '!=',  'CL555555555'),
                    ('commercial_partner_id', '!=', self.commercial_partner_id.id ),
                ],
                limit=1,
            )
            if exist:
                self.vat = ''
                self.document_number = ''
                return {'warning': {'title': 'Informacion para el Usuario',
                                    'message': _("El contacto %s está utilizando este numero de documento" ) % exist.name,
                                    }}
            self.vat = vat
            self.document_number = '%s.%s.%s-%s' % (
                                        document_number[0:2], document_number[2:5],
                                        document_number[5:8], document_number[-1],
                                    )
            otro = '%s%s%s-%s' % (
                                        document_number[0:2], document_number[2:5],
                                        document_number[5:8], document_number[-1],
                                    )
            try:
                if otro[:1] == '0':
                    otro=otro[1:]
                url = self.env['ir.config_parameter'].sudo().get_param('konos.url')+otro
                response = requests.get(url)
                partner = json.loads(response.text)
            except Exception:
                partner=False
                response=False

            if response==False:
                return

            if response.status_code!=200:
                _logger.warning("error %s" %(response))
                vals= {'detail':"Not found."}
            else:
                vals = response and response.json() or {'detail':"Not found."}
                if vals:
                    self.name = vals.get('name', '')
                    self.company_type="company"
                    self.country_id = self.env['res.country'].search([("code","=", 'CL')], limit=1)
                    self.dte_email = vals.get('email', '')
                    self.website = vals.get('url', '')


                    #TENEMOS QUE RECORRER LAS DIRECCIONES HASTA ENCONTRAR LA TYPE 1
                    if vals.get('address'):
                        if response.json().get("address")[0].get("address_type") in ['1']:
                            self.street = response.json().get("address")[0].get("street") + " " + response.json().get("address")[0].get("number")
                            self.street2 = response.json().get("address")[0].get("block") + " " + response.json().get("address")[0].get("department")+ " " + response.json().get("address")[0].get("villa")
                            busca_comuna = self._verificar_comuna(response.json().get("address")[0].get("commune"))
                            if busca_comuna:
                                self.city_id = busca_comuna.id
                        else:
                            self.street = response.json().get("address")[0].get("street") + " " + response.json().get("address")[0].get("number")
                            self.street2 = response.json().get("address")[0].get("block") + " " + response.json().get("address")[0].get("department")+ " " + response.json().get("address")[0].get("villa")
                            busca_comuna = self._verificar_comuna(response.json().get("address")[0].get("commune"))
                            if busca_comuna:
                                self.city_id = busca_comuna.id

                    if response.json().get("turns"):
                        giro_string = response.json().get("turns")[0].get("description")
                        start = giro_string.find('-') + 1
                        self.activity_description = self._buscar_giro(giro_string[start:])


        elif self.document_number and (
            'sii.document_type',
            self.document_type_id.id) == mod_obj.get_object_reference(
                'l10n_cl_fe',
                'dt_Sigd',
            ):
            self.document_number = ''
        else:
            self.vat = ''



    # Para apoyar el codigo de busqueda de la comuna con respecto a la API_RUT
    def _verificar_comuna(self, registro):
        if registro=='NUNOA':
            registro = 'Ñuñoa'
        elif registro=='CAMINA':
            registro = 'Camiña'
        elif registro=='PENALOLEN':
            registro = 'Peñalolén'
        elif registro=='PENAFLOR':
            registro = 'Peñaflor'
        elif registro=='VIVUNA':
            registro = 'Vicuña'
        elif registro=='VINA DEL MAR':
            registro = 'Viña del Mar'
        elif registro=='DONIHUE':
            registro = 'Doñihue'
        elif registro=='HUALANE':
            registro = 'Hualañé'
        elif registro=='CANETE':
            registro = 'Cañete'
        elif registro=='NIQUEN':
            registro = 'Ñiquén'
        elif registro=='RIO IBANEZ':
            registro = 'Río Ibáñez'
        elif registro=='CONCEPCION':
            registro = 'Concepción'
        elif registro=='CONCHALI':
            registro = 'Conchalí'
        elif registro=='PUERTO NATALES':
            registro = 'Natales'




        busca_comuna = self.env['res.city'].search(
                    [
                        ('name','=', registro.title()),
                    ],
                    limit=1,
                )
        _logger.warning("Comuna a ser buscada: (%s) - Se encontro: %s" %(registro.title(), busca_comuna))

        return busca_comuna


    def _buscar_giro(self, registro):
        giro = registro.strip().upper()

        busca_giro = self.env['sii.activity.description'].search(
                    [
                        ('name','=', giro),
                    ],
                    limit=1,
                )

        giro_retorno = False
        if busca_giro:
            giro_retorno = busca_giro.id
        else:
            # En caso de no encontrar la comuna
            ## # -----------------------------------------
            # Si el giro no existe se crea
            # En FE se debe agregar campos que faltan: Ejemplo codigo del sii
            # En el Excel hay que agregar si es: IVA afecto, no o ND
            giro_grabar = {}
            giro_grabar['name'] = giro
            #giro_grabar['vat_affected'] = 'SI'
            #giro_grabar['active'] = True
            giro_retorno = self.env['sii.activity.description'].create(giro_grabar)
            giro_retorno = giro_retorno.id
            ## # -----------------------------------------

        return giro_retorno


    @api.model
    def _arregla_str(self, texto, size=1):
        c = 0
        cadena = ""
        special_chars = [
         [u'á', 'a'],
         [u'é', 'e'],
         [u'í', 'i'],
         [u'ó', 'o'],
         [u'ú', 'u'],
         [u'ñ', 'n'],
         [u'Á', 'A'],
         [u'É', 'E'],
         [u'Í', 'I'],
         [u'Ó', 'O'],
         [u'Ú', 'U'],
         [u'Ñ', 'N']]

        while c < size and c < len(texto):
            cadena += texto[c]
            c += 1

        for char in special_chars:
          try:
            cadena = cadena.replace(char[0], char[1])
          except:
            pass
        return cadena

    @api.onchange('city_id')
    def _asign_city(self):
        if self.city_id:
            self.country_id = self.city_id.state_id.country_id.id
            self.state_id = self.city_id.state_id.id
            self.city = self.city_id.name

    @api.one
    def update_document(self):
        self.onchange_vat()

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if not recs:
            recs = self.search(['|','|',('name', operator, name),('document_number', operator, name),('vat', operator, name)] + args, limit=limit)
        return recs.name_get()
