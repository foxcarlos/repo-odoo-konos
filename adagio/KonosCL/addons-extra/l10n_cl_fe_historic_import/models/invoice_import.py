# -*- coding: utf-8 -*-
# Part of Konos. See LICENSE file for full copyright and licensing details.
import re
import tempfile
import binascii
import logging
from datetime import datetime, timedelta, date
from odoo.exceptions import Warning, UserError, ValidationError
from odoo import models, fields, api, exceptions, _
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import xlrd
from xlrd import xlsx


_logger = logging.getLogger(__name__)
try:
    import xlwt
except ImportError:
    _logger.debug('Cannot `import xlwt`.')
try:
    import cStringIO
except ImportError:
    _logger.debug('Cannot `import cStringIO`.')
try:
    import base64
except ImportError:
    _logger.debug('Cannot `import base64`.')


class account_invoice_import_wizards(models.TransientModel):
    _name= "account.invoice.import.history"
    _description= "Account Invoice Import History"

    file = fields.Binary('File')
    file_opt = fields.Selection([('excel','Excel'),('csv','CSV')], default='excel')
    product_id = fields.Many2one('product.product', 'Product', index=True)
    create_partner = fields.Boolean(
        string='Create Partner',
        help="Select if you want to create partner not found", default=False)
    invoice_opt = fields.Selection([('purchase','Purchase'),('sale','Sale')], default='sale')

    @api.multi
    def import_file(self):
        fp = tempfile.NamedTemporaryFile(suffix=".xlsx")
        fp.write(binascii.a2b_base64(self.file))
        fp.seek(0)
        values = {}
        workbook = xlrd.open_workbook(fp.name)
        sheet = workbook.sheet_by_index(0)
        contador = 0
        invoice_obj = self.env["account.invoice"]
        invoice_line_obj = self.env["account.invoice.line"]

        for row_no in range(sheet.nrows):
            if row_no > 2:
                line = list(map(lambda row:isinstance(row.value, str) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))
                #Colocaremos solo de Talca
                total_amount = 0
                net_amount = 0
                #if str(line[1]).replace(".0", "") == str(self.commercial_office):
                if net_amount == 0:

                    name = str(line[0].decode("utf-8")).title()

                    vat = str(line[1].decode("utf-8")).upper()
                    street = str(line[2].decode("utf-8")).title()
                    try:
                        email = str(line[3].decode("utf-8")).title()
                    except:
                        email = 'sincorreo@sincorreo.cl'
                    try:
                        mobile = str(line[4]).replace(".0", "")
                    except:
                        mobile = '569'

                    #branch  = str(line[4].decode("utf-8")).title()
                    doc_type = str(line[6].decode("utf-8")).title()
                    folio = int(str(line[7]).replace(".0", ""))
                    #excel_date = int(str(line[19].replace(".0", "")))
                    excel_date = int(str(line[8].replace(".0", "")))
                    date_invoice = datetime(*xlrd.xldate_as_tuple(excel_date, 0)).strftime(DEFAULT_SERVER_DATE_FORMAT)
                    #excel_date = int(str(line[20].replace(".0", "")))
                    excel_date = int(str(line[9].replace(".0", "")))
                    date_due = datetime(*xlrd.xldate_as_tuple(excel_date, 0)).strftime(DEFAULT_SERVER_DATE_FORMAT)
                    total_amount = int(str(line[12].replace(".0", "")))
                    vat = self.format_rut(vat)


                    _logger.warning("Total")
                    _logger.warning(total_amount)

                    if doc_type=='Factura Electrónica' or doc_type=='33' or doc_type=='Factura Electronica':
                        doc_type='33'
                    elif doc_type=='Nota de Credito Electronica' or doc_type=='Nota de Crédito Electrónica' or doc_type=='61':
                        doc_type='61'
                    elif doc_type=='Boleta Electrónica' or doc_type=='Boleta Electronica' or doc_type=='39':
                        doc_type='39'
                    elif doc_type=='Factura Exenta Electrónica' or doc_type=='Factura Exenta Electronica' or doc_type=='34':
                        doc_type='34'
                    else:
                        return

                    partner_id = self._find_partner(vat)



                    if not partner_id and self.create_partner:
                        #Si no consigo dirección de facturación.
                        company_type= 'company'
                        invoicing_policy='invoice'
                        if doc_type=='39':
                            company_type= 'person'
                            invoicing_policy='ticket'


                        data = {
                        'name' : name,
                        'street':street or '',
                        'email':email or '',
                        'mobile':mobile or '',
                        'vat':vat,
                        #'document_type_id': self.env.ref('l10n_cl_fe.dt_RUT').id,
                        #'responsability_id': self.env.ref('l10n_cl_fe.res_IVARI').id,
                        'company_type': company_type,
                        'invoicing_policy':invoicing_policy,
                        }


                        partner_id = self.env['res.partner'].create(data)
                        partner_id.document_number = vat.replace("CL", "")
                        partner_id.update_document()

                        try:
                            partner_id.comment = 'Cargado desde Cartera de Cliente'
                            partner_id.type = 'invoice'

                        except Exception as error:
                            _logger.log(25, "Partner Not Created: %s", repr(error))

                    if partner_id:
                        #Tipo de Factura
                        invoice_type = 'in_invoice'
                        if self.invoice_opt == "sale":
                            invoice_type = 'out_invoice'
                        if self.invoice_opt == 'sale':
                            journal_id = self.env["account.journal"].search([('code', '=', 'CXC')], limit=1)
                        else:
                            journal_id = self.env["account.journal"].search([('code', '=', 'CXP')], limit=1)


                        journal_document_class_id = self._get_journal(doc_type)
                        _logger.warning("Tipo Doc")
                        _logger.warning(journal_document_class_id.name)

                        #Revisar si Factura Existe
                        inv_exist = self.env['account.invoice'].search(
                            [
                                ('reference', '=', folio),
                                ('partner_id.vat', '=', vat),
                            ])
                        if inv_exist:
                            _logger.warning("Factura existente")
                        else:
                                        #Datos de la factura
                            curr_invoice = {
                                'origin' : "Carga Inicial",
                                'reference': folio,
                                'partner_id' : partner_id.id,
                                'state': 'draft',
                                'date_invoice': date_invoice,
                                'date_due':date_due,
                                'journal_id': journal_id.id,
                                'company_id': self.env.user.company_id.id,
                                'journal_document_class_id': journal_document_class_id.id,
                                'type': invoice_type,
                                        }

                            #Crea Factura
                            inv_ids = invoice_obj.create(curr_invoice)
                            total_total=total_amount

                            if self.product_id.property_account_income_id.id:
                                line_account = self.product_id.property_account_income_id.id
                            elif self.product_id.categ_id.property_account_income_categ_id:
                                line_account = self.product_id.categ_id.property_account_income_categ_id.id
                            elif journal_id.default_credit_account_id:
                                line_account = journal_id.default_credit_account_id.id
                            else:
                                account_type = self.env['account.account.type'].search([('name','=','Income')]).id
                                line_account = elf.env['account.account'].search([('user_type_id','=',account_type)], limit=1)
                            try:

                                net_amount = round(total_total/1.19)

                                query = [('sii_code', '=', '14'),('type_tax_use', '=', ('purchase' if self.invoice_opt == 'purchase' else 'sale'))]
                                taxes = self.env['account.tax'].search( query, limit=1)
                                linea = {
                                'product_id': self.product_id.id,
                                'account_id': line_account,
                                'name': self.product_id.name or '',
                                'price_unit': net_amount or 0,
                                'quantity': 1.0,
                                'invoice_line_tax_ids': [(6, 0, taxes.ids)],
                                'invoice_id': inv_ids.id,
                                'price_subtotal': net_amount or 0,
                                }
                                _logger.warning("CREAN DO LINEA")
                                _logger.warning(linea)
                                curr_invoice_line = invoice_line_obj.create(linea)
                            except Exception as error:
                                _logger.log(25, "Invoice Line Error: %s", repr(error))
                            try:
                                inv_ids.compute_taxes()
                                inv_ids.action_invoice_open()
                                inv_ids.move_id.name = inv_ids.number = folio
                                inv_ids.journal_document_class_id = journal_document_class_id.id
                                try:
                                    self._cr.execute(""" UPDATE account_move_line SET ref=%s
                                        WHERE move_id=%s AND (ref IS NULL OR ref = '/')""",
                                        (folio, inv_ids.move_id.id))
                                except:
                                    _logger.warning("Factura Repetida: %s", (folio))
                            except Exception as error:
                                _logger.log(25, "Invoice Tax Error: %s", repr(error))
                    else:
                        _logger.warning('No encuentra Razón Social Referencia Truck' + vat)


    def format_rut(self, RUTEmisor=None):
        rut = RUTEmisor.replace('-', '').replace('.', '')
        if rut:
            if int(rut[:-1]) < 10000000:
                try:
                    rut = '0' + str(int(rut))
                except:
                    rut = '0' + str(rut)
            rut = 'CL' + rut
        else:
            rut = 'CL55555555'
        return rut

    def _find_partner(self,rut):
        try:
            partner_id = self.env['res.partner'].search(
                [
                    ('active','=', True),
                    ('parent_id', '=', False),
                    ('vat','=', rut)
                ])
            #En caso de que el cliente se autofacture
            if partner_id.id ==self.env.user.company_id.id:
                return False
            if partner_id:
                return partner_id
            else:
                return False
        except:
            return False





    def _get_journal(self, sii_code):
        type = 'purchase'
        if self.invoice_opt == 'sale':
            type = 'sale'
        journal_sii = self.env['account.journal.sii_document_class'].search(
            [
                ('sii_document_class_id.sii_code', '=', sii_code),
            ],
            limit=1,
        )
        return journal_sii







class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.multi
    @api.constrains('is_company', 'invoicing_policy', 'parent_id')
    def check_invoicing_policy(self):
        for rec in self:
            root = rec
            while root.parent_id:
                root = root.parent_id
            if not root.is_company and rec.invoicing_policy == 'eguide':
                return
