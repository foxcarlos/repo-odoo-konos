# -*- coding: utf-8 -*-
{
    "name": """Importación de Registros de Venta\
    """,
    'version': '0.0.1',
    'category': 'Localization/Chile',
    'sequence': 12,
    'author':  'Konos',
    'website': 'https://konos.cl',
    'license': 'AGPL-3',
    'summary': '',
    'description': """
Importación de Registros de Venta.
""",
    'depends': [
            'l10n_cl_fe',
            'l10n_cl_invoicing_policy',
        ],

    'data': [
            'views/invoice_import.xml',
            'data/account_journal.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
