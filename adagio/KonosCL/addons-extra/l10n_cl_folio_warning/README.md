# Minimun Folio Warning
Keep users informed when the minimum level of folio's stock is reached.


## Table of contents
* [Configuration](#configuration)
* [Usage](#usage)
* [Credits](#credits)
  * [Authors](#authors)
  * [Contributors](#contributors)
  * [Maintainers](#maintainers)


## Configuration
Go to *Settings > Users & Companies > Companies*, select your company and set
the users that will receive the notifications.

![image_01](static/description/image_01.png)


## Usage
A warning email is sent automatically when the minimum folio stock level
is reached.

![image_02](static/description/image_02.png)

## Credits

### Authors
* Konos Soluciones & Servicios

### Contributors
* Nelson Ramirez <<nramirez@konos.cl>>
* Alexander Olivares <<aolivares@konos.cl>>

### Maintainers
This module is maintained by **Konos Soluciones & Servicios**.

Current maintainer:
* Nelson Ramirez <<nramirez@konos.cl>>
