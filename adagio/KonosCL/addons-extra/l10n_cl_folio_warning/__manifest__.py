# -*- coding: utf-8 -*-
{
    'name': "Minimun Folio Warning",

    'summary': """
        Keep users informed when the minimum level of folio's stock is reached.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Localization/Chile',
    'version': '12.0.0.0.1',

    'depends': [
        'l10n_cl_fe',
    ],

    'data': [
        'data/ir_cron.xml',
        'views/ir_sequence.xml',
        'views/res_company.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
