# -*- coding: utf-8 -*-

from odoo import models, fields


class ResCompany(models.Model):
    _inherit = "res.company"

    etd_responsable_ids = fields.Many2many(
        "res.users", string="Folios Responsable",
        help="Users to whom the warning notification will be sent when the "
        "minimum level of folio's stock is reached.")
