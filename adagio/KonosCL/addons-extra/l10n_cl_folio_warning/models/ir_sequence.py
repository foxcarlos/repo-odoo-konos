# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class IrSequence(models.Model):
    _inherit = "ir.sequence"

    min_level = fields.Integer(
        string="Minimum level", default=10,
        help="The minimum level of folio's stock allowed before sending a "
        "warning notification.")

    def _check_minimun(self):
        users = self.company_id.etd_responsable_ids
        if self.qty_available <= self.min_level and users:
            email_to = ','.join(users.mapped('login'))
            values = {
                'subject': _("Minimum folio stock level reached warning"),
                'email_to': email_to,
                'body_html': _("Please load folios for: %s") % (self.name),
            }
            send_mail = self.env['mail.mail'].sudo().create(values)
            send_mail.send()

    def check_minimum(self):
        self.ensure_one()
        if self.is_dte:
            self._check_minimun()

    @api.model
    def folio_warn(self):
        folios = self.env["ir.sequence"].search([("is_dte", "!=", False)])
        for record in folios:
            record.check_minimum()
