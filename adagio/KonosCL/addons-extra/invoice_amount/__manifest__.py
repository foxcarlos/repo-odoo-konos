# -*- coding: utf-8 -*-
{
    'name': "Invoice Amount",

    'summary': """
        Avoid to overpaid an invoice.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'account',
    ],

    'data': [
        'views/account_payment_view.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
