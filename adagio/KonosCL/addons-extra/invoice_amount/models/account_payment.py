# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import ValidationError


class account_abstract_payment(models.AbstractModel):
    _inherit = "account.abstract.payment"

    @api.one
    @api.constrains('amount')
    def _check_amount(self):
        active_ids = self._context.get('active_ids')
        invoices = self.env['account.invoice'].browse(active_ids)
        if invoices.type == 'out_invoice':
            currency = invoices[0].currency_id
            total_amount = self._compute_payment_amount(
                invoices=invoices, currency=currency)
            if self.amount < 0 or self.amount > total_amount:
                raise ValidationError(_('The payment amount is not correct.'))
