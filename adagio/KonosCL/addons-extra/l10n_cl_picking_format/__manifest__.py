# -*- coding: utf-8 -*-
{
    'name': "Delivery Guide Report templates",
    'summary': """
        Enables new custom report templates adapted to Chilean regulations.
    """,

    'author': "Odoolatam",
    'website': "https://odoolatam.odoo.com/",

    'category': 'Localization',
    'version': '12.0.0.0.1',

    'depends': [
        'l10n_cl_fe',
        'l10n_cl_stock_picking',
    ],

    'data': [
        'data/report.xml',
        'views/report_stock.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
