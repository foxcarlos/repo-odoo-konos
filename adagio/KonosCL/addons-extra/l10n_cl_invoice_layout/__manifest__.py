# -*- coding: utf-8 -*-
{
    'name': "Chile - Invoice Layouts For DTE 2.0",
    'summary': """
        Enables new custom layout adapted to Chilean regulations.
    """,

    'author': "Konos",
    'website': "https://konos.cl/",

    'category': 'Localization',
    'version': '12.0.0.0.1',

    'depends': [
        'l10n_cl_fe',
    ],

    'data': [
        'data/report.xml',
        'views/report_invoice.xml',

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
