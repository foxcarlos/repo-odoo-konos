# -*- coding: utf-8 -*-

from odoo import api, models


class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.model
    def create(self, vals):
        config_id = vals.get('config_id') or self.env.context.get(
            'default_config_id')
        if config_id:
            pos_config = self.env['pos.config'].browse(config_id)
            # Checking all the journal existed
            to_add_journals = []
            current_journals = pos_config.journal_ids
            for journal in current_journals:
                if journal.apply_rounding and \
                    journal.rounding_diff_journal_id not in \
                        current_journals:
                    to_add_journals.append(journal.rounding_diff_journal_id.id)

            to_add_journals = set(to_add_journals)
            if to_add_journals:
                pos_config.write({
                    'journal_ids':
                        [(4, journal_id) for journal_id in to_add_journals]})

        return super(PosSession, self).create(vals)
