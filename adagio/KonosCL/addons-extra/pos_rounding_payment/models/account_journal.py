# -*- coding: utf-8 -*-

from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    apply_rounding = fields.Boolean(string="Apply Rounding")
    rounding_method = fields.Selection(
        selection=[('rounding_up', 'Rounding Up'),
                   ('rounding_down', 'Rounding Down'),
                   ('rounding_off_up', 'Rounding Off (Priority: Up)'),
                   ('rounding_off_down', 'Rounding Off (Priority: Down)')],
        default='rounding_down',
        string="Rounding Method")
    rounding_factor = fields.Float(string="Rounding Factor")
    rounding_diff_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string="Journal for Diff Amount",
        help="The journal used for generating accounting " +
             "entries of the rounding different amount")
    show_round_jnl_on_payment = fields.Boolean(
        string="Show Journal on Payment",
        help="Show this journal for diff amount on Payment Screen")
