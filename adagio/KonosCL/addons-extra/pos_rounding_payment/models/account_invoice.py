# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def get_pos_payments(self):
        self.ensure_one()
        pos_order = self.env['pos.order'].search(
            [('invoice_id', '=', self.id)], limit=1)

        if pos_order and pos_order.statement_ids:
            # Check for rounding payments
            rounding_journal_ids = \
                pos_order.statement_ids.mapped('journal_id').filtered(
                    lambda x: x.apply_rounding and
                    x.rounding_diff_journal_id).mapped(
                        "rounding_diff_journal_id").ids

            statements = sorted(pos_order.statement_ids,
                                key=lambda x: x.journal_id.id
                                in rounding_journal_ids, reverse=True)
            return statements
        return False
