# -*- coding: utf-8 -*-
{
    'name': 'POS Rounding Payment',
    'version': '1.0',
    'category': 'Point of Sales',
    "license": "LGPL-3",
    'price': 15.00,
    'currency': 'EUR',
    'description': """
    """,
    'author': 'Felix',
    'depends': [
        'point_of_sale',
    ],
    'data': [
        'views/account_journal_view.xml',
        'views/point_of_sale_view.xml',
        'reports/account_invoice.xml',
    ],

    'test': [],
    'demo': [],
    'qweb': [
        'static/src/xml/pos.xml'
    ],

    'images': ['static/description/banner.png'],

    'installable': True,
    'active': True,
    'application': False,
}
