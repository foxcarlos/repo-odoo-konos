odoo.define('pos_rounding_payment.screens.payment_screen', function(require) {
    "use strict";

    var pos_screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');

    pos_screens.PaymentScreenWidget.include({
        click_paymentmethods: function(id) {
            // Checking if the selected support the rounding
            var rounding_journal_data = this.pos.rounding_journals[id];

            if (rounding_journal_data && rounding_journal_data.apply_rounding && !this.existing_rounding_payment_line()) {
                var order = this.pos.get_order();
                // Idenifying the rounding amount
                console.log("Enrto aqui")
                var payment_due = order.get_due();
                var rounding_factor = rounding_journal_data.rounding_factor;
                var new_amount = false;

                var amount_rounding_up = Math.ceil(payment_due / rounding_factor)*rounding_factor;
                var amount_rounding_down = Math.floor(payment_due / rounding_factor)*rounding_factor;
                var rounding_up_diff = Math.abs(payment_due - amount_rounding_up);
                var rounding_down_diff = Math.abs(payment_due - amount_rounding_down);

                switch(rounding_journal_data.rounding_method) {
                    case "rounding_up":
                        new_amount = amount_rounding_up;
                        break;
                    case "rounding_down":
                        new_amount = amount_rounding_down;
                        break;
                    case "rounding_off_up":
                        if (rounding_up_diff <= rounding_down_diff) {
                            new_amount = amount_rounding_up;
                        } else {
                            new_amount = amount_rounding_down;
                        }
                        break;
                    case "rounding_off_down":
                        if (rounding_down_diff <= rounding_up_diff) {
                            new_amount = amount_rounding_down;
                        } else {
                            new_amount = amount_rounding_up;
                        }
                        break;

                }

                var rounding_amount = payment_due - new_amount;
                if (rounding_amount != 0) {
                    // Create a new payment line with the rounding method
                    console.log(this.pos.cashregisters.length)
                    var rounding_journal = rounding_journal_data.rounding_diff_journal_id;
                    var cashregister = false;
                    for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
                        console.log(this.pos.cashregisters[i].journal_id[0])
                        console.log(rounding_journal[0])
                        if ( this.pos.cashregisters[i].journal_id[0] === rounding_journal[0] ){
                            cashregister = this.pos.cashregisters[i];
                            break;
                        }
                    }

                    // Adding payment line
                    order.assert_editable();
                    console.log(models);
                    var newPaymentline = new models.Paymentline({}, { order: order, cashregister: cashregister, pos: this.pos });
                    newPaymentline.set_amount(rounding_amount);
                    order.paymentlines.add(newPaymentline);
                    this.render_paymentlines();  
                }

            }

            return this._super(id);
        },

        payment_input: function(key) {
            var order = this.pos.get_order();
            var paymentline = order.selected_paymentline;
            if (paymentline && paymentline.is_editable()) {
                return this._super(key);
            }
        },

        click_back: function(){
            // Remove all payment lines on clicking back button
            var order = this.pos.get_order();
            var lines = order.get_paymentlines();
            var num_of_paymentlines = lines.length;
            for ( var i = 0; i < num_of_paymentlines; i++ ) {
                order.remove_paymentline(lines[0]);
            }
            this.reset_input();
            this.render_paymentlines();
            return this._super();
        },

        existing_rounding_payment_line: function() {
            var payment_lines = this.pos.get_order().get_paymentlines();
            var rounding_journal_ids = this.pos.rounding_journal_ids;
            for (var i = 0; i < payment_lines.length; i++) {
                if (rounding_journal_ids.includes(payment_lines[i].cashregister.journal_id[0])) {
                    return true;
                }
            }
            return false;
        },

    });

    return pos_screens.PaymentScreenWidget;
});
