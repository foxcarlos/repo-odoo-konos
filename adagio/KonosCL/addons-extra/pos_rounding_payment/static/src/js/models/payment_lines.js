odoo.define('pos_rounding_payment.models.paymentline', function(require) {
"use strict";

    var models = require('point_of_sale.models');

    var Paymentline = models.Paymentline;
    var _super  = Paymentline.prototype;

    models.Paymentline = Paymentline.extend({
        is_editable: function() {
            var journal = this.cashregister.journal;
            return !this.pos.rounding_journal_ids.includes(journal.id);
        },

 });

    return models.Paymentline;
});