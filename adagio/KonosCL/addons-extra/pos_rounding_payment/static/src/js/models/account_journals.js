odoo.define('pos_rounding_payment.pos', function (require) {
"use strict";

// var PosBaseWidget = require('point_of_sale.BaseWidget');
// var chrome = require('point_of_sale.chrome');
// var gui = require('point_of_sale.gui');
var models = require('point_of_sale.models');
// var screens = require('point_of_sale.screens');
// var core = require('web.core');

// var QWeb = core.qweb;
// var _t = core._t;

    models.load_models({
        model: 'account.journal',
        fields: ['apply_rounding', 'rounding_method', 'rounding_factor', 'rounding_diff_journal_id', 'show_round_jnl_on_payment'],
        domain: function(self){ return [['id', 'in', self.config.journal_ids]]; },
        loaded: function(self, journals){
            self.rounding_journals = {};
            self.rounding_journal_ids = [];
            self.rounding_journal_toshow = [];
            _.each(journals, function(journal){
                self.rounding_journals[journal.id] = journal;
                if (journal.apply_rounding && journal.rounding_diff_journal_id) {
                    self.rounding_journal_ids.push(journal.rounding_diff_journal_id[0]);

                    if (journal.show_round_jnl_on_payment) {
                        self.rounding_journal_toshow.push(journal.rounding_diff_journal_id[0]);
                    }
                }
            });
        },
    });
});