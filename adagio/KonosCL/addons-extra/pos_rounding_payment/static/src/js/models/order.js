odoo.define('pos_rounding_payment.models.order', function(require) {
"use strict";

    var models = require('point_of_sale.models');

    var Order = models.Order;
    var _super  = Order.prototype;

    models.Order = Order.extend({
        is_rounding_journal: function(journal_id) {
            return this.pos.rounding_journal_ids.includes(journal_id);
        },


        show_journal_on_payment: function(journal_id) {
            return !this.is_rounding_journal(journal_id) || this.pos.rounding_journal_toshow.includes(journal_id);

        }

 });

    return models.Order;
});