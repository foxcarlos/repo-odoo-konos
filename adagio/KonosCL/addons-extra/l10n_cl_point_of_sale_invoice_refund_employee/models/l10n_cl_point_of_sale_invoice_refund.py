# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import logging
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class POSInvoiceRefund(models.Model):
    _inherit = "l10n_cl_point_of_sale_invoice_refund"

    #Replace the whole create refund
    def create_refund(self):
        clone_list = []
        context = dict(self._context or {})

        current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)], limit=1)
        if not current_session:
            raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

        journal_document_class_id = self.env['account.journal.sii_document_class'].search(
            [('journal_id.type','=', 'sale'),
             ('sii_document_class_id.sii_code', 'in', [61])], limit=1)
        PosOrder = self.env['pos.order']
        PosOrder_line = self.env["pos.order.line"]


        amount = 0
        payments = self.account_invoice._get_payments_vals()
        for p in payments:
            amount += p['amount']


        employee_id = self.env['hr.employee'].search(
            [('user_id', '=', self.account_invoice.user_id.id)])

        values = {
            'name': self.account_invoice.name or '' + ' REFUND', # not used, name forced by create
            'partner_id': self.account_invoice.partner_id.id,
            'user_id': self.env.user.id,
            'cashier': self.env.user.name,
            'employee_id': employee_id.id,
            'session_id': current_session.id,
            'date_order': fields.Datetime.now(),
            'pos_reference': self.account_invoice.name,
            'lines': False,
            'amount_tax': -self.account_invoice.amount_tax,
            'amount_total': -self.account_invoice.amount_total,
            'amount_paid': 0,
            'amount_return': 0,
            'sequence_id': journal_document_class_id.sequence_id.id,
            'document_class_id': journal_document_class_id.sii_document_class_id.id,
            'sii_document_number': 0,
            'signature': False,
            'referencias': [[5,],[0,0, {
            'origen': int(self.account_invoice.sii_document_number),
            'sii_referencia_TpoDocRef': self.account_invoice.journal_document_class_id.sii_document_class_id.id,
            'sii_referencia_CodRef': self.filter_refund,
            'motivo': self.motivo,
            'fecha_documento': self.account_invoice.date_invoice,
            }]],
        }
        clone_id = self.env['pos.order'].create(values)

        for line in self.account_invoice.invoice_line_ids:
            clone_line = PosOrder_line.create({
                'name': line.name + _(' REFUND'),
                'order_id': clone_id.id,
                'qty': -line.quantity,
                'price_unit': line.price_unit,
                'product_id':line.product_id.id,
                'tax_ids':[(6, 0, line.invoice_line_tax_ids.ids)],
                'price_subtotal': -line.price_subtotal,
                'price_subtotal_incl': -line.price_subtotal,
                })
        PosOrder += clone_id

        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': PosOrder.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
