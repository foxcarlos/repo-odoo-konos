# -*- coding: utf-8 -*-
{
    "name": """POS Invoice Refund for Chile With Employees\
    """,
    'version': '0.0.1',
    'category': 'Localization/Chile',
    'sequence': 12,
    'author':  'Konos',
    'website': 'https://konos.cl',
    'license': 'AGPL-3',
    'summary': '',
    'description': """
POS Invoice Refund for Chile With Employees.
""",
    'depends': [
            'l10n_cl_point_of_sale_invoice_refund',
            'pos_hr',
],
    'installable': True,
    'auto_install': False,
    'application': True,
}
