## Third-party modules for Odoo

Additional features to improve Odoo.

* additional_reports: extra reports for Odoo modules.
* general_disable_create: disables _Create and Edit..._ for some critical fields.
* l10n_cl_clean_doc_number: clean document numbers and XML from DTE invoice.
* l10n_cl_currency_rate_sbif: update exchange rates from SBIF.
* l10n_cl_partner: autocompletes partner information using the Konos API.
* l10_cl_report: custom report templates adapted to Chilean regulations.
* shopify_ept: shopify Odoo Connector helps you in integrating and managing your Shopify store with Odoo.
    * auto_invoice_workflow_ept: automatic workflow settings.
    * common_connector_library: common connector library.
