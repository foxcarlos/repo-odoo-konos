# -*- coding: utf-8 -*-
##############################################################################
#    OpenERP, Open Source Management Solution
# 
##############################################################################

{
    'name': 'User Restrict Create Product and Contact',
    'version': '12.0.1.0',
    'category': 'product',
    'description': """
        User Restrict Create Product 
    """,
    'summary': """
        User Restrict Create Product, 
        now user restrict can not create product, \n
        now user not create sale order and purchase order  
        
    """,
    'author': 'Webline apps, Konos',
    'website': 'weblineapps@gmail.com ',
    'depends': ['sale','purchase'],
    'data': [
        'security/security.xml',
        'views/product_templet_view.xml',
        'views/res_partner_view.xml',
        'views/sale_order_view.xml',
        'views/purchase_order_View.xml',
    ],
    'images' : ['static/description/banner.png'],
    'installable': True,
    'auto_install': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
