# -*- coding: utf-8 -*-

from odoo import models, fields


class BukAccount(models.Model):
    _name = "buk.account"
    _description = "Buk Account"

    name = fields.Char(
        help="Buk account name.")
    code = fields.Char(
        help="Buk account code.")
    account_id = fields.Many2one(
        'account.account', string='Account',
        help="Equivalent account in Odoo.")
    company_id = fields.Many2one(
        'res.company', string='Company',
        help="Company associated with the account.")


class BukCostCenter(models.Model):
    _name = "buk.cost.center"
    _description = "Buk Cost Center"

    name = fields.Char(
        help="Buk cost center name.")
    code = fields.Char(
        help="Buk cost center code.")
    analytic_account_id = fields.Many2one(
        'account.analytic.account', string='Analytic Account',
        help="Equivalent analytic account in Odoo.")
    company_id = fields.Many2one(
        'res.company', string='Company',
        help="Company associated with the cost center.")


class BukImportedPeriod(models.Model):
    _name = "buk.imported.period"
    _description = "Buk Imported Period"

    month = fields.Char(
        help="Month consulted.")
    year = fields.Char(
        help="Year consulted.")
    company_id = fields.Many2one(
        'res.company', string='Company',
        help="Company associated with the imported period.")
