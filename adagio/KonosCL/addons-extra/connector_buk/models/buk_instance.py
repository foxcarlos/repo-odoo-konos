# -*- coding: utf-8 -*-

from odoo import models, fields


class BukInstance(models.Model):
    _name = "buk.instance"
    _description = "Buk Instance"

    api_endpoint = fields.Char(
        string="Url", help="Accounting Centralization Endpoint.")
    api_token = fields.Char(
        string="Token", help="Token generated on the platform.")
    account_id = fields.Many2one(
        'account.account', string='Default Account',
        help="This account will be used if there is no coincidence for a Buk "
        "account during the import process.")
    analytic_account_id = fields.Many2one(
        'account.analytic.account', string='Default Analytic Account',
        help="This analytic account will be used if there is no coincidence "
        "for a Buk cost center during the import process.")
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id.id,
        help="Company associated with the instance.")
