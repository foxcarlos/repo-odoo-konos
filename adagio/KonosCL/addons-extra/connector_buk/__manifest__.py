# -*- coding: utf-8 -*-
{
    'name': "Buk Connector",

    'summary': """
        Import the accounting information for a specific month and year.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Accounting/Accounting',
    'version': '12.0.0.0.1',

    'depends': [
        'account_accountant',
        'l10n_cl_fe',
    ],

    'data': [
        'security/ir.model.access.csv',
        'security/buk_security.xml',
        'wizard/buk_entries.xml',
        'views/buk_account_views.xml',
        'views/buk_instance_views.xml',
        'views/buk_menu.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
