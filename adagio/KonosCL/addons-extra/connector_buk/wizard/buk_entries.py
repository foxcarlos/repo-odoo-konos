# -*- coding: utf-8 -*-

import json
import requests
from odoo import models, fields, _
from odoo.exceptions import ValidationError, UserError


class BukEntriesWizard(models.Model):
    _name = "buk.entries.wizard"
    _description = "Buk Entries Wizard"

    month = fields.Integer(
        help="Month to consult.")
    year = fields.Integer(
        help="Year to consult.")
    journal_id = fields.Many2one(
        'account.journal', string='Journal',
        help="Journal where the entries will be generated.")

    def send_request(self, page=1):
        """Send a GET request to the Buk API"""
        instance = self.env['buk.instance'].search([
            ('company_id', '=', self.env.user.company_id.id)], limit=1)
        endpoint = instance.api_endpoint
        token = instance.api_token

        if not endpoint or not token:
            raise ValidationError(_(
                "Before import entries, you must provide the API info. "
                "Please go to General Settings > Payroll > BUK Integration."))

        headers = {'Accept': 'application/json', 'auth_token': token}
        company = self.env.user.company_id.document_number.replace(
            '.', '').replace('-', '')
        params = {
            'month': self.month,
            'year': self.year,
            'company_id': company,
            'page': page,
        }
        try:
            response = requests.get(endpoint, params=params, headers=headers)
            response.raise_for_status()
            content = response.json()
        except requests.exceptions.HTTPError:
            raise UserError(_("The information could not be retrieved."))
        return content

    def matching_account(self, code):
        """Searches for the equivalent of the Buk account in the Odoo
        accounts, or the account set as default if no match is found"""
        account = self.env['buk.account'].search([
            ('code', '=', code)]).account_id
        if not account:
            instance = self.env['buk.instance'].search([
                ('company_id', '=', self.env.user.company_id.id)], limit=1)
            account = instance.account_id
        return account

    def matching_analytic(self, code):
        """Searches for the equivalent of the Buk cost center in the
        Odoo analytic accounts, or the analytic account set as default
        if no match is found"""
        account = self.env['buk.cost.center'].search([
            ('code', '=', code)]).analytic_account_id
        if not account:
            instance = self.env['buk.instance'].search([
                ('company_id', '=', self.env.user.company_id.id)], limit=1)
            account = instance.analytic_account_id
        return account

    def matching_partner(self, rut):
        """Searches for an employee or creates it if it does not exist"""
        partner = self.env['res.partner'].search([
            ('document_number', '=', rut)])
        if not partner:
            partner = self.env['res.partner'].create({
                'name': 'Trabajador (%s)' % (rut),
                'document_number': rut,
            })
        return partner

    def action_import_entries(self):
        """Imports into Odoo the entries of the consulted month and year"""
        already_imported = self.env['buk.imported.period'].search_count([
            ('month', '=', self.month), ('year', '=', self.year),
            ('company_id', '=', self.env.user.company_id.id)
        ])
        if already_imported:
            raise ValidationError(_(
                "This period was already imported into Odoo."))

        content = self.send_request()
        pages = content['pagination']['total_pages']
        data = []
        # REVIEW: too much for loops here! This should be improved.
        for page in range(1, pages + 1):
            content = self.send_request(page)
            data.extend(content['data'])

        for group in data:
            if group['items']:
                values = {
                    'journal_id': self.journal_id.id,
                    'date': fields.Date.today(),
                    'ref': group['id'],
                    'line_ids': [],
                }
                for item in group['items']:
                    account_id = self.matching_account(item['account'])
                    partner_id = self.matching_partner(item['employee_rut'])
                    analytic_id = self.matching_analytic(item['cost_center'])
                    debit = (item['amount']
                             if item['entry_type'] == 'debit' else 0.0)
                    credit = (item['amount']
                              if item['entry_type'] == 'credit' else 0.0)
                    values['line_ids'].append((0, 0, {
                        'account_id': account_id.id,
                        'partner_id': partner_id.id,
                        'name': item['description'],
                        'analytic_account_id': analytic_id.id,
                        'debit': debit,
                        'credit': credit,
                    }))
                self.env['account.move'].create(values)

        self.env['buk.imported.period'].create({
            'month': self.month,
            'year': self.year,
            'company_id': self.env.user.company_id.id,
        })
