# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import ValidationError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_open(self):
        if self.type == 'out_refund' and self.origin:
            invoice = self.env['account.invoice'].search([
                ('number', '=', self.origin)])
            refunds = self.env['account.invoice'].search([
                ('origin', '=', self.origin), ('id', '!=', self.id)])
            total_refunds = abs(sum(refunds.mapped('amount_total')))
            current_total = abs(self.amount_total)

            if total_refunds + current_total > invoice.amount_total:
                raise ValidationError(_(
                    "El monto de la nota de crédito excede el monto total "
                    "de la orden inicial"))
        return super(AccountInvoice, self).action_invoice_open()
