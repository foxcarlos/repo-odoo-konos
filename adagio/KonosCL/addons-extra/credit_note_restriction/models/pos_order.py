# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import ValidationError


class PosOrder(models.Model):
    _inherit = 'pos.order'

    def payment_validation(self):
        ticket_doctype = self.env.ref('l10n_cl_fe.dc_b_f_dte')
        refund_doctype = self.env.ref('l10n_cl_fe.dc_nc_f_dte')
        if self.document_class_id.id == refund_doctype.id:
            orders = self.env['pos.order'].search([
                ('pos_reference', '=', self.pos_reference)])
            ticket = orders.filtered(
                lambda r: r.document_class_id.id == ticket_doctype.id)
            refunds = orders.filtered(
                lambda r: r.document_class_id.id == refund_doctype.id
                and r.id != self.id)
            total_refunds = abs(sum(refunds.mapped('amount_total')))
            current_total = abs(self.amount_total)

            if total_refunds + current_total > ticket.amount_total:
                raise ValidationError(_(
                    "El monto de la nota de crédito excede el monto total "
                    "de la orden inicial"))
        action = self.env.ref('point_of_sale.action_pos_payment').read()[0]
        return action
