# -*- coding: utf-8 -*-
{
    'name': "Credit Note Restriction",

    'summary': """
        Apply some validations before validating a refund.
    """,

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Sales',
    'version': '0.1',

    'depends': [
        'l10n_cl_fe',
        'l10n_cl_dte_point_of_sale',
    ],

    'data': [
        'views/pos_order_views.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
