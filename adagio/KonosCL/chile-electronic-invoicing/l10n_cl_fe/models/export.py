# -*- coding: utf-8 -*-
from odoo import models
from datetime import datetime

class LibroXlsx(models.AbstractModel):
    _name = 'report.account.move.book.xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, libro):
        for obj in libro:
            report_name = obj.name
            # One sheet by partner
            sheet = workbook.add_worksheet(report_name[:31])
            bold = workbook.add_format({'bold': True})
            sheet.write(0, 0, obj.name, bold)
            sheet.write(0, 1, obj.company_id.name, bold)
            sheet.write(0, 2, obj.periodo_tributario, bold)
            sheet.write(0, 3, obj.tipo_operacion, bold)
            sheet.write(0, 4, obj.tipo_libro, bold)
            sheet.write(0, 5, obj.tipo_operacion, bold)
            sheet.write(2, 0, u"Tipo de Documento", bold)
            sheet.write(2, 1, u"Número", bold)
            sheet.write(2, 2, u"Fecha Emisión", bold)
            sheet.write(2, 3, u"RUT", bold)
            sheet.write(2, 4, u"Entidad", bold)
            sheet.write(2, 5, u"Afecto", bold)
            sheet.write(2, 6, u"Exento", bold)
            sheet.write(2, 7, u"IVA", bold)
            sheet.write(2, 8, u"Total", bold)
            line = 3
            neto = 0
            exento = 0
            iva = 0
            total = 0
            for mov in obj.move_ids:
                if mov.sii_document_number:
                    sheet.write(line, 0, mov.document_class_id.name)
                    sheet.write(line, 1, (mov.sii_document_number or mov.ref))
                    sheet.write(line, 2, mov.date.strftime('%d-%m-%Y'))
                    if mov.partner_id:
                        sheet.write(line, 3, mov.partner_id.document_number or '55.555.555-5')
                        sheet.write(line, 4, mov.partner_id.name or 'Consumidor Final')
                    else:
                        sheet.write(line, 3, "")
                        sheet.write(line, 4, "")
                    totales = mov.totales_por_movimiento()

                    neto_line = totales['neto']
                    exento_line = totales['exento']
                    amount_line = mov.amount
                    if mov.document_class_id.sii_code in [60, 61]:
                        amount_line = amount_line * -1
                        neto_line = neto_line * -1
                        exento_line = exento_line * -1

                    sheet.write(line, 5, neto_line)
                    sheet.write(line, 6, exento_line)
                    sheet.write(line, 7, totales['iva'])
                    sheet.write(line, 8, amount_line)
                    neto += neto_line
                    exento += exento_line
                    iva += totales['iva']
                    total += amount_line
                    line += 1
                else:
                    pos_invoice_obj = self.env['pos.order'].search([('session_id','=',mov.ref),('document_class_id.sii_code', 'in', [39, 41])], limit=1)
                    for boletas in pos_invoice_obj:
                        sheet.write(line, 0, boletas.document_class_id.name)
                        sheet.write(line, 1, (boletas.sii_document_number or ' '))
                        sheet.write(line, 2, boletas.date_order.strftime('%d-%m-%Y'))
                        if boletas.partner_id:
                            sheet.write(line, 3, boletas.partner_id.document_number or '55.555.555-5')
                            sheet.write(line, 4, boletas.partner_id.name or 'Consumidor Final')
                        else:
                            sheet.write(line, 3, "")
                            sheet.write(line, 4, "")
                        
                        exento_line = 0
                        amount_line = boletas.amount_total
                        amount_tax = boletas.amount_tax
                        neto_line = amount_line - amount_tax

                        sheet.write(line, 5, neto_line)
                        sheet.write(line, 6, exento_line)
                        sheet.write(line, 7, amount_tax)
                        sheet.write(line, 8, amount_line)
                        neto += neto_line
                        exento += exento_line
                        iva += amount_tax
                        total += amount_line
                        line += 1

                                                
                        
            sheet.write(line, 0, "Total General", bold)
            sheet.write(line, 5, neto, bold)
            sheet.write(line, 6, exento, bold)
            sheet.write(line, 7, iva, bold)
            c = 8
            if obj.total_otros_imps > 0:
                sheet.write(line, c , obj.total_otros_imps, bold)
                c +=1
            #sheet.write(line, c, obj.total_no_rec, bold)
            sheet.write(line, c, total, bold)


class HonorarioXlsx(models.AbstractModel):
    _name = 'report.account.move.book.honorarios.xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, libro):
        for obj in libro:
            report_name = obj.name
            sheet = workbook.add_worksheet(report_name[:31])
            bold = workbook.add_format({'bold': True})
            sheet.write(0, 0, obj.name, bold)
            sheet.write(0, 1, obj.company_id.name, bold)
            sheet.write(0, 2, obj.tipo_libro, bold)
            sheet.write(0, 3, obj.periodo_tributario, bold)
            sheet.write(2, 0, u"Tipo de Documento", bold)
            sheet.write(2, 1, u"Número", bold)
            sheet.write(2, 2, u"Fecha Emisión", bold)
            sheet.write(2, 3, u"RUT", bold)
            sheet.write(2, 4, u"Entidad", bold)
            sheet.write(2, 5, u"Bruto", bold)
            sheet.write(2, 6, u"Retención", bold)
            sheet.write(2, 7, u"Líquido", bold)
            line = 3
            total = 0
            retencion = 0
            bruto = 0
            
            
            for mov in obj.move_ids:
                sheet.write(line, 0, mov.document_class_id.name)
                sheet.write(line, 1, (mov.sii_document_number or mov.ref))
                sheet.write(line, 2, mov.date.strftime('%d-%m-%Y'))
                partner_doc = mov.partner_id.document_number or ""
                partner_name = mov.partner_id.name or ""
                sheet.write(line, 3, partner_doc)
                sheet.write(line, 4, partner_name)
                
                account_invoice_obj = self.env['account.invoice'].search([('move_id','=',mov.id)])
                sheet.write(line, 5, mov.amount)
                sheet.write(line, 6, account_invoice_obj.amount_retencion)
                sheet.write(line, 7, account_invoice_obj.amount_total)
                bruto += mov.amount
                retencion += account_invoice_obj.amount_retencion
                total += account_invoice_obj.amount_total
                line += 1
            sheet.write(line, 5, bruto, bold)
            sheet.write(line, 6, retencion, bold)
            sheet.write(line, 7, total, bold)
