# -*- coding: utf-8 -*-
import dicttoxml
from lxml import etree
import base64
import hashlib
from lxml.etree import Element, SubElement
import OpenSSL
from OpenSSL import crypto
type_ = crypto.FILETYPE_PEM
import textwrap


try:
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives.serialization import load_pem_private_key
    import OpenSSL
    from OpenSSL import crypto
    type_ = crypto.FILETYPE_PEM
except:
    _logger.warning('Cannot import OpenSSL library')


from odoo import fields, models, api, tools
from odoo.tools.translate import _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import dateutil.relativedelta as relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
import collections
import pytz
import logging
_logger = logging.getLogger(__name__)
try:
    from facturacion_electronica import facturacion_electronica as fe
    from facturacion_electronica.consumo_folios import ConsumoFolios as CF
except Exception as e:
    _logger.warning("Problema al cargar Facturación electrónica: %s" % str(e))

import os, sys
USING_PYTHON2 = True if sys.version_info < (3, 0) else False
xsdpath = os.path.dirname(os.path.realpath(__file__)).replace('/models','/static/xsd/')


class ConsumoFolios(models.Model):
    _name = "account.move.consumo_folios"
    _description = 'Consumo de Folios SII'

    sii_xml_request = fields.Many2one(
            'sii.xml.envio',
            string='SII XML Request',
            copy=False,
            readonly=True,
            states={'draft': [('readonly', False)]},)
    state = fields.Selection([
            ('draft', 'Borrador'),
            ('NoEnviado', 'No Enviado'),
            ('EnCola', 'En Cola'),
            ('Enviado', 'Enviado'),
            ('Aceptado', 'Aceptado'),
            ('Rechazado', 'Rechazado'),
            ('Reparo', 'Reparo'),
            ('Proceso', 'Proceso'),
            ('Reenviar', 'Reenviar'),
            ('Anulado', 'Anulado')],
        string='Resultado',
        index=True,
        readonly=True,
        default='draft',
        track_visibility='onchange',
        copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Pro-forma' status is used the invoice does not have an invoice number.\n"
             " * The 'Open' status is used when user create invoice, an invoice number is generated. Its in open status till user does not pay invoice.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")
    move_ids = fields.Many2many(
        'account.move',
        readonly=True,
        states={'draft': [('readonly', False)]},)
    fecha_inicio = fields.Date(
            string="Fecha Inicio",
            readonly=True,
            states={'draft': [('readonly', False)]},
            default=lambda self: fields.Date.context_today(self),
        )
    fecha_final = fields.Date(
            string="Fecha Final",
            readonly=True,
            states={'draft': [('readonly', False)]},
            default=lambda self: fields.Date.context_today(self),
        )
    correlativo = fields.Integer(
            string="Correlativo",
            readonly=True,
            states={'draft': [('readonly', False)]},
            invisible=True,
        )
    sec_envio = fields.Integer(
            string="Secuencia de Envío",
            readonly=True,
            states={'draft': [('readonly', False)]},
        )
    total_neto = fields.Monetary(
        string="Total Neto",
        store=True,
        readonly=True,
        compute='get_totales',)
    total_iva = fields.Monetary(
        string="Total Iva",
        store=True,
        readonly=True,
        compute='get_totales',)
    total_exento = fields.Monetary(
        string="Total Exento",
        store=True,
        readonly=True,
        compute='get_totales',)
    total = fields.Monetary(
        string="Monto Total",
        store=True,
        readonly=True,
        compute='get_totales',)
    total_boletas = fields.Integer(
        string="Total Boletas",
        store=True,
        readonly=True,
        compute='get_totales',)
    company_id = fields.Many2one(
        'res.company',
        required=True,
        default=lambda self: self.env.user.company_id.id,
        readonly=True,
        states={'draft': [('readonly', False)]},)
    name = fields.Char(
        string="Detalle" ,
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},)
    date = fields.Date(
        string="Date",
        required=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        default=lambda self: fields.Date.context_today(self),
    )
    detalles = fields.One2many(
        'account.move.consumo_folios.detalles',
        'cf_id',
        string="Detalle Rangos",
        readonly=True,
        states={'draft': [('readonly', False)]},)
    impuestos = fields.One2many(
        'account.move.consumo_folios.impuestos',
        'cf_id',
        string="Detalle Impuestos",
        readonly=True,
        states={'draft': [('readonly', False)]},)
    anulaciones = fields.One2many(
        'account.move.consumo_folios.anulaciones',
        'cf_id',
        string="Folios Anulados",
        readonly=True,
        states={'draft': [('readonly', False)]},)
    currency_id = fields.Many2one(
        'res.currency',
        string='Moneda',
        default=lambda self: self.env.user.company_id.currency_id,
        required=True,
        track_visibility='always',
        readonly=True,
        states={'draft': [('readonly', False)]},
    )
    responsable_envio = fields.Many2one(
        'res.users',
    )
    sii_result = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('NoEnviado', 'No Enviado'),
            ('Enviado', 'Enviado'),
            ('Aceptado', 'Aceptado'),
            ('Rechazado', 'Rechazado'),
            ('Reparo', 'Reparo'),
            ('Proceso', 'Proceso'),
            ('Reenviar', 'Reenviar'),
            ('Anulado', 'Anulado')
        ],
        related="state",
    )

    _order = 'fecha_inicio desc'

    def xml_validator(self, some_xml_string, validacion='doc'):
        validacion_type = {
            'consu': 'ConsumoFolio_v10.xsd',
            'sig': 'xmldsignature_v10.xsd',
        }
        xsd_file = xsdpath+validacion_type[validacion]
        try:
            xmlschema_doc = etree.parse(xsd_file)
            xmlschema = etree.XMLSchema(xmlschema_doc)
            xml_doc = etree.fromstring(some_xml_string)
            result = xmlschema.validate(xml_doc)
            if not result:
                xmlschema.assert_(xml_doc)
            return result
        except AssertionError as e:
            raise UserError(_('XML Malformed Error:  %s') % e.args)

    def ensure_str(self,x, encoding="utf-8", none_ok=False):
        if none_ok is True and x is None:
            return x
        if not isinstance(x, str):
            x = x.decode(encoding)
        return x

    def digest(self, data):
        sha1 = hashlib.new('sha1', data)
        return sha1.digest()

    def sign_full_xml(self, message, privkey, cert, uri, type='consu'):
        doc = etree.fromstring(message)
        string = etree.tostring(doc[0])
        mess = etree.tostring(etree.fromstring(string), method="c14n")
        digest = base64.b64encode(self.digest(mess))
        reference_uri='#'+uri
        signed_info = Element("SignedInfo")
        c14n_method = SubElement(signed_info, "CanonicalizationMethod", Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315')
        sign_method = SubElement(signed_info, "SignatureMethod", Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1')
        reference = SubElement(signed_info, "Reference", URI=reference_uri)
        transforms = SubElement(reference, "Transforms")
        SubElement(transforms, "Transform", Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315")
        digest_method = SubElement(reference, "DigestMethod", Algorithm="http://www.w3.org/2000/09/xmldsig#sha1")
        digest_value = SubElement(reference, "DigestValue")
        digest_value.text = digest
        signed_info_c14n = etree.tostring(signed_info,method="c14n",exclusive=False,with_comments=False,inclusive_ns_prefixes=None)
        att = 'xmlns="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
        #@TODO Find better way to add xmlns:xsi attrib
        signed_info_c14n = signed_info_c14n.decode().replace("<SignedInfo>","<SignedInfo %s>" % att)
        sig_root = Element("Signature",attrib={'xmlns':'http://www.w3.org/2000/09/xmldsig#'})
        sig_root.append(etree.fromstring(signed_info_c14n))
        signature_value = SubElement(sig_root, "SignatureValue")
        key = crypto.load_privatekey(type_,privkey.encode('ascii'))
        signature = crypto.sign(key,signed_info_c14n,'sha1')
        signature_value.text =textwrap.fill(base64.b64encode(signature).decode(),64)
        key_info = SubElement(sig_root, "KeyInfo")
        key_value = SubElement(key_info, "KeyValue")
        rsa_key_value = SubElement(key_value, "RSAKeyValue")
        modulus = SubElement(rsa_key_value, "Modulus")
        key = load_pem_private_key(privkey.encode('ascii'),password=None, backend=default_backend())
        longs = self.env['account.move.book'].long_to_bytes(key.public_key().public_numbers().n)
        modulus.text =  textwrap.fill(base64.b64encode(longs).decode(),64)
        exponent = SubElement(rsa_key_value, "Exponent")
        longs = self.env['account.move.book'].long_to_bytes(key.public_key().public_numbers().e)
        exponent.text = self.ensure_str(base64.b64encode(longs).decode())
        x509_data = SubElement(key_info, "X509Data")
        x509_certificate = SubElement(x509_data, "X509Certificate")
        x509_certificate.text = '\n'+textwrap.fill(cert,64)
        msg = etree.tostring(sig_root).decode()
        msg = msg if self.xml_validator(msg, 'sig') else ''
        fulldoc = message.replace('</ConsumoFolios>',msg+'\n</ConsumoFolios>')
        fulldoc = fulldoc
        fulldoc = '<?xml version="1.0" encoding="ISO-8859-1"?>\n'+fulldoc if self.xml_validator(fulldoc, type) else ''
        return fulldoc

    def create_template_env(self, doc,simplificado=False):
        xsd = 'http://www.sii.cl/SiiDte ConsumoFolio_v10.xsd'
        xml = '''<ConsumoFolios xmlns="http://www.sii.cl/SiiDte" \
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
xsi:schemaLocation="{0}" \
version="1.0">
{1}</ConsumoFolios>'''.format(xsd, doc)
        return xml

    def time_stamp(self, formato='%Y-%m-%dT%H:%M:%S'):
        tz = pytz.timezone('America/Santiago')
        return datetime.now(tz).strftime(formato)

    def get_resolution_data(self, comp_id):
        resolution_data = {
            'dte_resolution_date': comp_id.dte_resolution_date,
            'dte_resolution_number': comp_id.dte_resolution_number}
        return resolution_data


    def create_template_envio(self, RutEmisor, FchResol, NroResol, FchInicio, FchFinal, Correlativo, SecEnvio, EnvioDTE, signature_d, IdEnvio='SetDoc'):
        if Correlativo != 0:
            Correlativo = "<Correlativo>"+str(Correlativo)+"</Correlativo>"
        else:
            Correlativo = ''
        xml = '''<DocumentoConsumoFolios ID="{10}">
<Caratula  version="1.0" >
<RutEmisor>{0}</RutEmisor>
<RutEnvia>{1}</RutEnvia>
<FchResol>{2}</FchResol>
<NroResol>{3}</NroResol>
<FchInicio>{4}</FchInicio>
<FchFinal>{5}</FchFinal>{6}
<SecEnvio>{7}</SecEnvio>
<TmstFirmaEnv>{8}</TmstFirmaEnv>
</Caratula>
{9}
</DocumentoConsumoFolios>
'''.format(RutEmisor, signature_d['subject_serial_number'],
           FchResol, NroResol, FchInicio, FchFinal, str(Correlativo), str(SecEnvio), self.time_stamp(), EnvioDTE,  IdEnvio)
        return xml

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        res = super(ConsumoFolios, self).read_group(domain, fields, groupby, offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'total_iva' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    line.update({
                            'total_neto': 0,
                            'total_iva': 0,
                            'total_exento': 0,
                            'total': 0,
                            'total_boletas': 0,
                        })
                    for l in lines:
                        line.update({
                                'total_neto': line['total_neto'] + l.total_neto,
                                'total_iva': line['total_iva'] + l.total_iva,
                                'total_exento': line['total_exento'] + l.total_exento,
                                'total': line['total'] + l.total,
                                'total_boletas': line['total_boletas'] + l.total_boletas,
                            })
        return res

    @api.onchange('impuestos')
    @api.depends('impuestos')
    def get_totales(self):
        for r in self:
            total_iva = 0
            total_exento = 0
            total = 0
            total_boletas = 0
            for d in r.impuestos:
                total_iva += d.monto_iva
                total_exento += d.monto_exento
                total += d.monto_total
            for d in r.detalles:
                if d.tpo_doc.sii_code in [39, 41] and d.tipo_operacion == "utilizados":
                    total_boletas += d.cantidad
            r.total_neto = total - total_iva - total_exento
            r.total_iva = total_iva
            r.total_exento = total_exento
            r.total = total
            r.total_boletas = total_boletas

    def _es_iva(self, tax):
        if tax.sii_code in [14, 15, 17, 18, 19, 30,31, 32 ,33, 34, 36, 37, 38, 39, 41, 47, 48]:
            return True
        return False

    def _nuevo_rango(self, folio, f_contrario, contrarios):
        last = self._last(folio, contrarios)#obtengo el último tramo de los contrarios
        if last and last['Inicial'] > f_contrario:
            return True
        return False

    def _last(self, folio, items):# se asumen que vienen ordenados de menor a mayor
        last = False
        for c in items:
            if folio > c['Final'] and folio > c['Inicial']:
                if not last or last['Inicial'] < c['Inicial']:
                    last = c
        return last

    def _orden(self, folio, rangos, contrarios, continuado=True):
        last = self._last(folio, rangos)
        if not continuado or not last or  self._nuevo_rango(folio, last['Final'], contrarios):
            r = collections.OrderedDict()
            r['Inicial'] = folio
            r['Final'] = folio
            rangos.append(r)
            return rangos
        result = []
        for r in rangos:
            if r['Final'] == last['Final'] and folio > last['Final']:
                r['Final'] = folio
            result.append(r)
        return result

    def _rangosU(self, resumen, rangos, continuado=True):
        if not rangos:
            rangos = collections.OrderedDict()
        folio = resumen['NroDoc']
        if 'Anulado' in resumen and resumen['Anulado']:
            utilizados = rangos['itemUtilizados'] if 'itemUtilizados' in rangos else []
            if not 'itemAnulados' in rangos:
                rangos['itemAnulados'] = []
                r = collections.OrderedDict()
                r['Inicial'] = folio
                r['Final'] = folio
                rangos['itemAnulados'].append(r)
            else:
                rangos['itemAnulados'] = self._orden(resumen['NroDoc'], rangos['itemAnulados'], utilizados, continuado)
            return rangos
        anulados = rangos['itemAnulados'] if 'itemAnulados' in rangos else []
        if not 'itemUtilizados' in rangos:
            rangos['itemUtilizados'] = []
            r = collections.OrderedDict()
            r['Inicial'] = folio
            r['Final'] = folio
            rangos['itemUtilizados'].append(r)
        else:
            rangos['itemUtilizados'] = self._orden(resumen['NroDoc'], rangos['itemUtilizados'], anulados, continuado)
        return rangos

    def _setResumen(self,resumen,resumenP,continuado=True):
        resumenP['TipoDocumento'] = resumen['TpoDoc']
        if not 'Anulado' in resumen:
            if 'MntNeto' in resumen and not 'MntNeto' in resumenP:
                resumenP['MntNeto'] = resumen['MntNeto']
            elif 'MntNeto' in resumen:
                resumenP['MntNeto'] += resumen['MntNeto']
            elif not 'MntNeto' in resumenP:
                resumenP['MntNeto'] = 0
            if 'MntIVA' in resumen and not 'MntIva' in resumenP:
                resumenP['MntIva'] = resumen['MntIVA']
            elif 'MntIVA' in resumen:
                resumenP['MntIva'] += resumen['MntIVA']
            elif not 'MntIva' in resumenP:
                resumenP['MntIva'] = 0
            if 'TasaIVA' in resumen and not 'TasaIVA' in resumenP:
                resumenP['TasaIVA'] = resumen['TasaIVA']
            if 'MntExe' in resumen and not 'MntExento' in resumenP:
                resumenP['MntExento'] = resumen['MntExe']
            elif 'MntExe' in resumen:
                resumenP['MntExento'] += resumen['MntExe']
            elif not 'MntExento' in resumenP:
                resumenP['MntExento'] = 0
        if not 'MntTotal' in resumenP:
            resumenP['MntTotal'] = resumen.get('MntTotal', 0)
        else:
            resumenP['MntTotal'] += resumen.get('MntTotal', 0)
        if 'FoliosEmitidos' in resumenP:
            resumenP['FoliosEmitidos'] +=1
        else:
            resumenP['FoliosEmitidos'] = 1

        if not 'FoliosAnulados' in resumenP:
            resumenP['FoliosAnulados'] = 0
        if 'Anulado' in resumen : # opción de indiar de que está anulado por panel SII no por nota
            resumenP['FoliosAnulados'] += 1
        elif 'FoliosUtilizados' in resumenP:
            resumenP['FoliosUtilizados'] += 1
        else:
            resumenP['FoliosUtilizados'] = 1
        if not resumenP.get('FoliosUtilizados', False):
            resumenP['FoliosUtilizados'] = 0
        if not str(resumen['TpoDoc'])+'_folios' in resumenP:
            resumenP[str(resumen['TpoDoc'])+'_folios'] = collections.OrderedDict()
        resumenP[str(resumen['TpoDoc'])+'_folios'] = self._rangosU(resumen, resumenP[str(resumen['TpoDoc'])+'_folios'], continuado)
        return resumenP

    def getResumen(self, rec):
        det = collections.OrderedDict()
        det['TpoDoc'] = rec.document_class_id.sii_code
        det['NroDoc'] = int(rec.sii_document_number)
        for a in self.anulaciones:
            if a.rango_inicio <= det['NroDoc'] and det['NroDoc'] <= a.rango_final and a.tpo_doc.id == rec.document_class_id.id:
                rec.canceled = True
        if rec.canceled:
            det['Anulado'] = 'A'
            return det
        Neto = 0
        MntExe = 0
        TaxMnt = 0
        MntTotal = 0
        if 'lines' in rec:
            # NC pasar a positivo
            TaxMnt =  rec.amount_tax if rec.amount_tax > 0 else rec.amount_tax * -1
            MntTotal = rec.amount_total if rec.amount_total > 0 else rec.amount_total * -1
            Neto = rec.pricelist_id.currency_id.round(sum(line.price_subtotal for line in rec.lines))
            if Neto < 0:
                Neto *= -1
            MntExe = rec.exento()
            TasaIVA = self.env['pos.order.line'].search([('order_id', '=', rec.id), ('tax_ids.amount', '>', 0)], limit=1).tax_ids.amount
            Neto -= MntExe
        else:  # si la boleta fue hecha por contabilidad
            for l in rec.line_ids:
                if l.tax_line_id:
                    if l.tax_line_id and l.tax_line_id.amount > 0: #supuesto iva único
                        if self._es_iva(l.tax_line_id): # diferentes tipos de IVA retenidos o no
                            if l.credit > 0:
                                TaxMnt += l.credit
                            else:
                                TaxMnt += l.debit
                elif l.tax_ids and l.tax_ids[0].amount > 0:
                    if l.credit > 0:
                        Neto += l.credit
                    else:
                        Neto += l.debit
                elif l.tax_ids and l.tax_ids[0].amount == 0: #caso monto exento
                    if l.credit > 0:
                        MntExe += l.credit
                    else:
                        MntExe += l.debit
            TasaIVA = self.env['account.move.line'].search([('move_id', '=', rec.id), ('tax_line_id.amount', '>', 0)], limit=1).tax_line_id.amount
            MntTotal = Neto + MntExe + TaxMnt
        if MntExe > 0 :
            det['MntExe'] = self.currency_id.round(MntExe)
        if TaxMnt > 0:
            det['MntIVA'] = self.currency_id.round(TaxMnt)
            det['TasaIVA'] = TasaIVA
        det['MntNeto'] = self.currency_id.round(Neto)
        det['MntTotal'] = self.currency_id.round(MntTotal)
        return det


    def _get_resumenes(self, marc=False):
        resumenes = collections.OrderedDict()
        TpoDocs = []
        recs = []
        for rec in self.with_context(lang='es_CL').move_ids:
            document_class_id = rec.document_class_id if 'document_class_id' in rec else rec.sii_document_class_id
            if not document_class_id or document_class_id.sii_code not in [39, 41]:
                _logger.warning("Por este medio solamente se pueden declarar Boletas o Notas de crédito Electrónicas, por favor elimine el documento %s del listado" % rec.name)
                continue
            if rec.sii_document_number:
                recs.append(rec)
            #rec.sended = marc
        if 'pos.order' in self.env: # @TODO mejor forma de verificar si está instalado módulo POS
            current = self.fecha_inicio.strftime(DF) + ' 00:00:00'
            tz = pytz.timezone('America/Santiago')
            tz_current = tz.localize(datetime.strptime(current, DTF)).astimezone(pytz.utc)
            current = tz_current.strftime(DTF)
            next_day = (tz_current + relativedelta.relativedelta(days=1)).strftime(DTF)
            orders_array = self.env['pos.order'].search(
                [
                 ('invoice_id' , '=', False),
                 ('sii_document_number', 'not in', [False, '0']),
                 ('document_class_id.sii_code', 'in', [39, 41]),
                 ('date_order','>=', current),
                 ('date_order','<', next_day),
                ]
            ).with_context(lang='es_CL')
            for order in orders_array:
                recs.append(order)
        if recs:
            recs = sorted(recs, key=lambda t: int(t.sii_document_number))
            ant = {}
            for order in recs:
                canceled = (hasattr(order, 'canceled') and order.canceled)
                resumen = self.getResumen(order)
                TpoDoc = str(resumen['TpoDoc'])
                if TpoDoc not in ant:
                    ant[TpoDoc] = [0, canceled]
                if int(order.sii_document_number) == ant[TpoDoc][0]:                    
                    values = {
                    'email_to': 'soporte@konos.cl',
                    'email_from': self.company_id.dte_email_id.name_get()[0][1],
                    'body_html': 'Error en Consumo de Folios',
                    'subject': "¡El Folio %s está duplicado!" % order.sii_document_number,
                    }
                    send_mail = self.env['mail.mail'].sudo().create(values)
                    send_mail.send()
                    raise UserError("¡El Folio %s está duplicado!" % order.sii_document_number)
                if TpoDoc not in TpoDocs:
                    TpoDocs.append(TpoDoc)
                if TpoDoc not in resumenes:
                    resumenes[TpoDoc] = collections.OrderedDict()
                continuado = ((ant[TpoDoc][0]+1) == int(order.sii_document_number) and (ant[TpoDoc][1]) == canceled)
                resumenes[TpoDoc] = self._setResumen(resumen, resumenes[TpoDoc], continuado)
                ant[TpoDoc] = [int(order.sii_document_number), canceled]
        for an in self.anulaciones:
            TpoDoc = str(an.tpo_doc.sii_code)
            if TpoDoc not in TpoDocs:
                TpoDocs.append(TpoDoc)
            if TpoDoc not in resumenes:
                resumenes[TpoDoc] = collections.OrderedDict()
            i = an.rango_inicio
            while i <= an.rango_final:
                continuado  = False
                seted = False
                for r, value in resumenes.items():
                    Rangos = value.get(str(r)+'_folios', collections.OrderedDict())
                    if 'itemAnulados' in Rangos:
                        for rango in Rangos['itemAnulados']:
                            if rango['Inicial'] <= i and i <= rango['Final']:
                                seted = True
                            if not(seted) and (i-1) == rango['Final']:
                                    continuado = True
                if not seted:
                    resumen = {
                        'TpoDoc': TpoDoc,
                        'NroDoc': i,
                        'Anulado': 'A',
                    }
                    if not resumenes.get(TpoDoc):
                        resumenes[TpoDoc] = collections.OrderedDict()
                    resumenes[TpoDoc] = self._setResumen(resumen, resumenes[TpoDoc], continuado)
                i += 1
        return resumenes, TpoDocs


    @api.onchange('move_ids', 'anulaciones')
    def _resumenes(self):
        resumenes, TpoDocs = self._get_resumenes()
        if self.impuestos and isinstance(self.id, int):
            self._cr.execute("DELETE FROM account_move_consumo_folios_impuestos WHERE cf_id=%s", (self.id,))
            self.invalidate_cache()
        if self.detalles and isinstance(self.id, int):
            self._cr.execute("DELETE FROM account_move_consumo_folios_detalles WHERE cf_id=%s", (self.id,))
            self.invalidate_cache()
        detalles = [[5,],]
        def pushItem(key_item, item, tpo_doc):
            rango = {
                'tipo_operacion': 'utilizados' if key_item == 'RangoUtilizados' else 'anulados',
                'folio_inicio': item['Inicial'],
                'folio_final': item['Final'],
                'cantidad': int(item['Final']) - int(item['Inicial']) +1,
                'tpo_doc': self.env['sii.document_class'].search([('sii_code', '=', tpo_doc)]).id,
            }
            detalles.append([0,0,rango])
        for r, value in resumenes.items():
            if '%s_folios' %str(r) in value:
                Rangos = value[ str(r)+'_folios' ]
                if 'itemUtilizados' in Rangos:
                    for rango in Rangos['itemUtilizados']:
                        pushItem('RangoUtilizados', rango, r)
                if 'itemAnulados' in Rangos:
                    for rango in Rangos['itemAnulados']:
                        pushItem('RangoAnulados', rango, r)
        self.detalles = detalles
        docs = collections.OrderedDict()
        for r, value in resumenes.items():
            if value.get('FoliosUtilizados', False):
                docs[r] = {
                       'tpo_doc': self.env['sii.document_class'].search([('sii_code','=', r)]).id,
                       'cantidad': value['FoliosUtilizados'],
                       'monto_neto': value['MntNeto'],
                       'monto_iva': value['MntIva'],
                       'monto_exento': value['MntExento'],
                       'monto_total': value['MntTotal'],
                       }
        lines = [[5,],]
        for key, i in docs.items():
            i['currency_id'] = self.env.user.company_id.currency_id.id
            lines.append([0,0, i])
        self.impuestos = lines

    @api.onchange('fecha_inicio', 'company_id', 'fecha_final')
    def set_data(self):
        self.move_ids = False
        current = datetime.now().strftime('%Y-%m-%d') + ' 00:00:00'
        tz = pytz.timezone('America/Santiago')
        tz_current = tz.localize(datetime.strptime(current, DTF)).astimezone(pytz.utc)
        current = tz_current.strftime(DTF)
        #fi = datetime.strptime(self.fecha_inicio + " 00:00:00", DTF)
        #if fi > datetime.strptime(current, DTF):
        #    raise UserError("No puede hacer Consumo de Folios de días futuros")
        self.name = self.fecha_inicio
        self.fecha_final = self.fecha_inicio
        self.move_ids = self.env['account.move'].search([
            ('document_class_id.sii_code', 'in', [39, 41]),
#            ('sended','=', False),
            ('date', '=', self.fecha_inicio),
            ('company_id', '=', self.company_id.id),
            ]).ids
        consumos = self.search_count([
            ('fecha_inicio', '=', self.fecha_inicio),
            ('state', 'not in', ['draft', 'Rechazado']),
            ('company_id', '=', self.company_id.id),
            ])
        self.sec_envio = 0
        if consumos > 0:
            self.sec_envio = (consumos+1)
        self._resumenes()

    @api.multi
    def copy(self, default=None):
        res = super(ConsumoFolios, self).copy(default)
        res.set_data()
        return res

    @api.multi
    def unlink(self):
        for cf in self:
            if cf.state not in ('draft', 'cancel'):
                raise UserError(_('You cannot delete a Validated Consumo de Folios.'))
        return super(ConsumoFolios, self).unlink()

    @api.multi
    def get_xml_file(self):
        return {
            'type' : 'ir.actions.act_url',
            'url': '/download/xml/cf/%s' % (self.id),
            'target': 'self',
        }

    @api.multi
    def validar_consumo_folios(self):
        self._validar()
        consumos = self.search([
            ('fecha_inicio', '=', self.fecha_inicio),
            ('state', 'not in', ['draft', 'Rechazado', 'Anulado']),
            ('company_id', '=', self.company_id.id),
            ('id', '!=', self.id),
            ])
        for r in consumos:
            r.state = "Anulado"
        return self.write({'state': 'NoEnviado'})

    def format_vat(self, value):
        if not value or value=='' or value == 0:
            value ="CL666666666"
            #@TODO opción de crear código de cliente en vez de rut genérico
        rut = value[:10] + '-' + value[10:]
        rut = rut.replace('CL0','').replace('CL','')
        return rut

    def _emisor(self):
        Emisor = {}
        Emisor['RUTEmisor'] = self.format_vat(self.company_id.vat)
        Emisor['RznSoc'] = self.company_id.name
        Emisor["Modo"] = "produccion" if self.company_id.dte_service_provider == 'SII'\
                  else 'certificacion'
        Emisor["NroResol"] = self.company_id.dte_resolution_number
        Emisor["FchResol"] = self.company_id.dte_resolution_date
        Emisor["ValorIva"] = 19
        return Emisor

    def _get_datos_empresa(self, company_id):
        signature_id = self.env.user.get_digital_signature(company_id)
        if not signature_id:
            raise UserError(_('''There are not a Signature Cert Available for this user, please upload your signature or tell to someelse.'''))
        emisor = self._emisor()
        return {
            "Emisor": emisor,
            "firma_electronica": signature_id.parametros_firma(),
        }

    def _get_moves(self):
        recs = []
        for rec in self.with_context(lang='es_CL').move_ids:
            rec.sended = True
            document_class_id = rec.document_class_id
            if not document_class_id or document_class_id.sii_code not in [39, 41]\
                or rec.sii_document_number in [False, 0]:
                continue
            ref = self.env['account.invoice'].search([
                ('sii_document_number', '=', rec.sii_document_number),
                ('document_class_id', '=', document_class_id.id),
                ('partner_id.commercial_partner_id', '=', rec.partner_id.id),
                ('journal_id', '=', rec.journal_id.id),
                ('state', 'not in', ['cancel', 'draft']),
                ('company_id', '=',rec.company_id.id),
            ])
            recs.append(ref)
        return recs

    def _validar(self):
        BC = '''-----BEGIN CERTIFICATE-----\n'''
        EC = '''\n-----END CERTIFICATE-----\n'''

        cant_doc_batch = 0
        company_id = self.company_id
        dte_service = company_id.dte_service_provider
        signature_d = self.env.user.get_digital_signature(self.company_id)
        if not signature_d:
            raise UserError(_('''There is no Signer Person with an \
        authorized signature for you in the system. Please make sure that \
        'user_signature_key' module has been installed and enable a digital \
        signature, for you or make the signer to authorize you to use his \
        signature.'''))
        certp = signature_d['cert'].replace(
            BC, '').replace(EC, '').replace('\n', '')
        resumenes, TpoDocs = self._get_resumenes(marc=True)
        Resumen=[]
        listado = [ 'TipoDocumento', 'MntNeto', 'MntIva', 'TasaIVA', 'MntExento', 'MntTotal', 'FoliosEmitidos',  'FoliosAnulados', 'FoliosUtilizados', 'itemUtilizados' ]
        xml = '<Resumen><TipoDocumento>39</TipoDocumento><MntTotal>0</MntTotal><FoliosEmitidos>0</FoliosEmitidos><FoliosAnulados>0</FoliosAnulados><FoliosUtilizados>0</FoliosUtilizados></Resumen>'
        if resumenes:
            for r, value in resumenes.items():
                ordered = collections.OrderedDict()
                for i in listado:
                    if i in value:
                        ordered[i] = value[i]
                    elif i == 'itemUtilizados':
                        Rangos = value[ str(r)+'_folios' ]
                        folios = []
                        if 'itemUtilizados' in Rangos:
                            utilizados = []
                            for rango in Rangos['itemUtilizados']:
                                utilizados.append({'RangoUtilizados': rango})
                            folios.append({'itemUtilizados': utilizados})
                        if 'itemAnulados' in Rangos:
                            anulados = []
                            for rango in Rangos['itemAnulados']:
                                anulados.append({'RangoAnulados': rango})
                            folios.append({'itemAnulados': anulados})
                        ordered[ str(r)+'_folios' ] = folios
                Resumen.extend([ {'Resumen': ordered}])
            dte = collections.OrderedDict({'item':Resumen})
            xml = dicttoxml.dicttoxml(
                dte,
                root=False,
                attr_type=False).decode()
        resol_data = self.get_resolution_data(company_id)
        RUTEmisor = self.format_vat(company_id.vat)
        RUTRecep = "60803000-K" # RUT SII
        doc_id =  'CF_'+self.date.strftime(DF)
        Correlativo = self.correlativo
        SecEnvio = self.sec_envio
        cf = self.create_template_envio( RUTEmisor,
            resol_data['dte_resolution_date'],
            resol_data['dte_resolution_number'],
            self.fecha_inicio,
            self.fecha_final,
            Correlativo,
            SecEnvio,
            xml,
            signature_d,
            doc_id)
        xml  = self.create_template_env(cf)
        root = etree.XML( xml )
        xml_pret = etree.tostring(root, pretty_print=True).decode()\
                .replace('<item>','\n').replace('</item>','')\
                .replace('<itemNoRec>','').replace('</itemNoRec>','\n')\
                .replace('<itemOtrosImp>','').replace('</itemOtrosImp>','\n')\
                .replace('<itemUtilizados>','').replace('</itemUtilizados>','\n')\
                .replace('<itemAnulados>','').replace('</itemAnulados>','\n')
        for TpoDoc in TpoDocs:
        	xml_pret = xml_pret.replace('<key name="'+str(TpoDoc)+'_folios">','').replace('</key>','\n').replace('<key name="'+str(TpoDoc)+'_folios"/>','\n')
        envio_dte = self.sign_full_xml(
            xml_pret,
            signature_d['priv_key'],
            certp,
            doc_id,
            'consu')
        doc_id += '.xml'
        self.sii_xml_request = self.env['sii.xml.envio'].create({
            'xml_envio': envio_dte,
            'name': doc_id,
            'company_id': self.company_id.id,
        }).id

    @api.multi
    def do_dte_send_consumo_folios(self):
        if self.state not in ['draft', 'NoEnviado', 'Rechazado']:
            raise UserError("El Consumo de Folios ya ha sido enviado")
        if not self.sii_xml_request or self.sii_xml_request.state == "Rechazado":
            if self.sii_xml_request:
                self.sii_xml_request.unlink()
            self._validar()
        self.env['sii.cola_envio'].create(
                    {
                        'company_id': self.company_id.id,
                        'doc_ids': [self.id],
                        'model': 'account.move.consumo_folios',
                        'user_id': self.env.user.id,
                        'tipo_trabajo': 'envio',
                    })
        self.state = 'EnCola'

    def do_dte_send(self, n_atencion=''):
        if self.sii_xml_request and self.sii_xml_request.state == "Rechazado":
            self.sii_xml_request.unlink()
            self._validar()
            self.sii_xml_request.state = 'NoEnviado'
        if self.state in ['NoEnviado', 'EnCola']:
            self.sii_xml_request.send_xml()
        return self.sii_xml_request

    def _get_send_status(self):
        self.sii_xml_request.get_send_status()
        if self.sii_xml_request.state == 'Aceptado':
            self.state = "Proceso"
        else:
            self.state = self.sii_xml_request.state

    @api.multi
    def ask_for_dte_status(self):
        self._get_send_status()

    def get_sii_result(self):
        for r in self:
            if r.sii_xml_request.state == 'NoEnviado':
                r.state = 'EnCola'
                continue
            r.state = r.sii_xml_request.state


class DetalleConsumoFolios(models.Model):
    _name = "account.move.consumo_folios.detalles"
    _description = 'Linea Detalle CF'

    cf_id = fields.Many2one(
        'account.move.consumo_folios',
        string="Consumo de Folios",)
    tpo_doc = fields.Many2one(
        'sii.document_class',
        string="Tipo de Documento",)
    tipo_operacion = fields.Selection([
        ('utilizados','Utilizados'),
        ('anulados','Anulados'),
    ])
    folio_inicio = fields.Integer(
        string="Folio Inicio",)
    folio_final = fields.Integer(
        string="Folio Final",)
    cantidad = fields.Integer(
        string="Cantidad Emitidos",)


class DetalleImpuestos(models.Model):
    _name = "account.move.consumo_folios.impuestos"
    _description = 'Detalle Impuesto CF'

    cf_id = fields.Many2one(
        'account.move.consumo_folios',
        string="Consumo de Folios",)
    tpo_doc = fields.Many2one(
        'sii.document_class',
        string="Tipo de Documento")
    impuesto = fields.Many2one(
        'account.tax',
        string="Impuesto",)
    cantidad = fields.Integer(
        string="Cantidad")
    monto_neto = fields.Monetary(
        string="Monto Neto")
    monto_iva = fields.Monetary(
        string="Monto IVA",)
    monto_exento = fields.Monetary(
        string="Monto Exento",)
    monto_total = fields.Monetary(
        string="Monto Total",)
    currency_id = fields.Many2one(
        'res.currency',
        string='Moneda',
        default=lambda self: self.env.user.company_id.currency_id,
        required=True,
        track_visibility='always',)


class Anulaciones(models.Model):
    _name = 'account.move.consumo_folios.anulaciones'
    _description = 'Detalle Folio Anulado CF'

    cf_id = fields.Many2one(
            'account.move.consumo_folios',
            string="Consumo de Folios",
        )
    tpo_doc = fields.Many2one(
            'sii.document_class',
            string="Tipo de documento",
            required=True,
            domain=[('sii_code','in',[ 39 , 41])],
        )
    rango_inicio = fields.Integer(
        required=True,
        string="Rango Inicio")
    rango_final = fields.Integer(
        required=True,
        string="Rango Final",)
