# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval as eval
from odoo.exceptions import UserError
import time
import logging
_logger = logging.getLogger(__name__)


class AccountInvoiceRefund(models.TransientModel):
    """Refunds invoice"""

    _name = "pos.order.refund"
    _description = 'pos.order.refund'

    tipo_nota = fields.Many2one(
            'sii.document_class',
            string="Tipo De nota",
            required=True,
            domain=[('document_type','in',['debit_note','credit_note']), ('dte','=',True)],
        )
    filter_refund = fields.Selection(
            [
                ('1','Anula Documento de Referencia'),
                ('2','Corrige texto Documento Referencia'),
                ('3','Corrige montos'),
            ],
            default='1',
            string='Refund Method',
            required=True, help='Refund base on this type. You can not Modify and Cancel if the invoice is already reconciled',
        )
    motivo = fields.Char("Motivo")
    date_order = fields.Date(string="Fecha de Documento")

    @api.multi
    def confirm(self):
        """Create a copy of order  for refund order"""
        clone_list = []
        line_obj = self.env['pos.order.line']
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for order in self.env['pos.order'].browse(active_ids):
            if order.amount_paid < 0 and self.tipo_nota.document_type=='credit_note':
                raise UserError("Odoo no permite hacer una Nota de crédito sobre Nota de crédito")
            if order.amount_paid > 0 and self.tipo_nota.document_type=='debit_note':
                raise UserError("Odoo no permite hacer una Nota de Débito sobre Facturas o Notas de Débito")
            if not order.document_class_id or not order.sii_document_number:
                raise UserError("Por esta área solamente se puede crear Nota de Crédito a Boletas validamente emitidas, si es un pedido simple, debe presionar en retornar simple")
            current_session_ids = self.env['pos.session'].search(
                    [
                        ('state', '!=', 'closed'),
                        ('user_id', '=', self.env.user.id),
                    ]
                )
            if not current_session_ids:
                raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))
            jdc_ob = self.env['account.journal.sii_document_class']
            journal_document_class_id = jdc_ob.search(
                    [
                        ('journal_id','=', order.sale_journal.id),
                        ('sii_document_class_id.sii_code', '=', self.tipo_nota.sii_code),
                    ],
                )
            if not journal_document_class_id:
                raise UserError("Por favor defina Secuencia de Notas de Crédito para el Journal del POS")
            clone_id = order.copy( {
                'name': order.name + ' REFUND', # not used, name forced by create
                'session_id': current_session_ids[0].id,
                'date_order': fields.Datetime.now(),
                'pos_reference': order.pos_reference,
                'amount_tax': -order.amount_tax,
                'amount_total': -order.amount_total,
                'amount_paid': 0,
                'sequence_id': journal_document_class_id.sequence_id.id,
                'document_class_id': journal_document_class_id.sii_document_class_id.id,
                'sii_document_number': 0,
                'signature': False,
                'referencias':[[5,],[0,0, {
                    'origen': int(order.sii_document_number),
                    'sii_referencia_TpoDocRef': order.document_class_id.id,
                    'sii_referencia_CodRef': self.filter_refund,
                    'motivo': self.motivo,
                    'fecha_documento': self.date_order
                }]],
            })
            clone_list.append(clone_id.id)
        clone_list = self.env['pos.order'].browse(clone_list)
        for clone in clone_list:
            for order_line in clone.lines:
                order_line.write( {
                    'qty': -order_line.qty,
                    'price_subtotal': -order_line.price_subtotal,
                    'price_subtotal_incl': -order_line.price_subtotal_incl,
                })
        abs = {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id':clone_list.id,
            'view_id': False,
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return abs
