# -*- coding: utf-8 -*-
##############################################################################
#
#    Konos
#    Copyright (C) 2020 Konos (<http://konos.cl/>)
#
##############################################################################

from odoo.tools.misc import str2bool, xlwt
from xlsxwriter.workbook import Workbook
import base64
import re,sys
import io
from odoo import api, fields, models
from xlwt import easyxf
import csv
import time
from datetime import datetime, date
import re

import logging
_logger = logging.getLogger(__name__)



class etd_account_excel_wizard_form(models.TransientModel):
    _name ='wizard.export.etd.document'
    _description = 'wizard.export.etd.document'

    date_to = fields.Date('Date To', required=True, default=lambda self: str(datetime.now()))
    date_from = fields.Date('Date From', required=True, default=lambda self: time.strftime('%Y-%m-01'))
    accounting_date = fields.Boolean(
        string='Accounting Date',
        help="")

    show_status = fields.Boolean(
        string='Show Status',
        help="Helps checking SII Result")

    operation_type = fields.Selection(
            [
                ('Compra','Compras'),
                ('Venta','Ventas'),
            ],
            string="Tipo de operación",
            default="Venta",
            required=True,
        )

    include_salesperson = fields.Boolean(
        string='Include Salesperson',
        help="Checking will include the salesperson in the last column")



    @api.onchange('date_to')
    def onchange_date_to(self):
        self.date_from = self.date_to.strftime('%Y-%m-01')


    def etd_document_excel(self):
        i = 1
        sheetName = 1
        workbook = xlwt.Workbook()

        n = 2
        c = 0
        style1 = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                              'font: colour white, bold True;')
        filename = 'Registrode'+str(self.operation_type)+str(self.date_to)+'.xls'
        style = xlwt.XFStyle()
        tall_style = xlwt.easyxf('font:height 720;') # 36pt
        font = xlwt.Font()
        font.name = 'Times New Roman'
        font.bold = True
        font.height = 250
        currency = xlwt.easyxf('font: height 180; align: wrap yes, horiz right',num_format_str='#,##0.00')
        date_format=xlwt.easyxf(num_format_str='DD-MM-YY')
        period_format=xlwt.easyxf(num_format_str='YYYY-MM')
        worksheet = workbook.add_sheet(str(self.operation_type))
        pos_etd_document_obj = False

        if not self.accounting_date:
            invoice_domain = [('date_invoice','>=',self.date_from),
                ('date_invoice','<=',self.date_to),
                ('state' ,'not in',['canceled','draft'])]
        else:
            invoice_domain = [('date','>=',self.date_from),
                ('date','<=',self.date_to),
                ('state' ,'not in',['canceled','draft'])]



        #All Documents Except Honorarios
        invoice_domain += [('document_class_id.sii_code', 'not in', [35, 38, 70, 71])]
        #else:
            #Only Boletas
        #    invoice_domain += [('document_class_id.sii_code', 'in', [35, 38, 39, 41])]

        #POS with ETD
        pos_etd_document_obj = self.env['pos.order'].sudo().search([('document_class_id.sii_code', '!=', False),
            ('date_order','>=',self.date_from),
            ('date_order','<=',self.date_to)] , order='date_order asc')


        if self.operation_type in ['Compra']:
            invoice_domain += [('type', 'in', ['in_invoice', 'in_refund'])]
        else:
            invoice_domain += [('type', 'in', ['out_invoice', 'out_refund'])]


        etd_document_obj = self.env['account.invoice'].sudo().search(invoice_domain, order='document_class_id, number, date_invoice asc')

        warehouse = self.env['stock.warehouse'].sudo().search([])

        worksheet.col(0).width = 3000
        worksheet.col(1).width = 6000
        worksheet.col(2).width = 3500
        worksheet.col(3).width = 5000
        worksheet.col(4).width = 5000
        worksheet.col(5).width = 15000
        worksheet.col(6).width = 5000
        worksheet.col(7).width = 5000
        worksheet.col(8).width = 5000
        worksheet.col(9).width = 4000
        worksheet.col(10).width = 4000
        worksheet.col(11).width = 4000
        worksheet.col(12).width = 5000
        worksheet.col(13).width = 4500
        worksheet.col(14).width = 6000
        worksheet.col(15).width = 5000
        worksheet.col(16).width = 7000
        worksheet.col(17).width = 7000
        worksheet.col(18).width = 7000
        if self.include_salesperson:
            worksheet.col(19).width = 8000
            worksheet.merge(0, 0, 0, 19, style1)
        else:
            worksheet.merge(0, 0, 0, 18, style1)



        worksheet.write(n-2, 0, 'Registro de '+str(self.operation_type), style1)

        worksheet.write(n-1, 0, 'Periodo', style1)
        worksheet.write(n-1, 1, 'Tipo Doc SII', style1)
        worksheet.write(n-1, 2, 'Folio Doc SII', style1)
        worksheet.write(n-1, 3, 'Folio Interno', style1)
        worksheet.write(n-1, 4, 'RUT Cliente', style1)
        worksheet.write(n-1, 5, 'Nombre Cliente', style1)
        worksheet.write(n-1, 6, 'Fecha del Documento', style1)
        worksheet.write(n-1, 7, 'Fecha del Asiento', style1)
        worksheet.write(n-1, 8, 'Fecha Vencimiento', style1)
        worksheet.write(n-1, 9, 'Exento', style1)
        worksheet.write(n-1, 10, 'Neto', style1)
        worksheet.write(n-1, 11, 'IVA', style1)
        worksheet.write(n-1, 12, 'Monto Total', style1)
        worksheet.write(n-1, 13, 'Vendedor', style1)
        worksheet.write(n-1, 14, 'Sucursal', style1)
        worksheet.write(n-1, 15, 'Estado SII', style1)
        worksheet.write(n-1, 16, 'Estado Pago', style1)
        worksheet.write(n-1, 17, 'Forma de Pago', style1)
        worksheet.write(n-1, 18, 'Monto TBK', style1)
        if self.include_salesperson:
            worksheet.write(n-1, 19, 'Vendedor Cliente', style1)

        exempt = 0
        amount_untaxed = 0
        amount_tax = 0
        amount_total =0
        amount_tbk_total = 0
        amount_tax_not_rec = 0

        for rec in etd_document_obj:
            if rec.document_class_id and rec.number:
                journal_names = " "
                tbk_amount = 0
                payments = rec._get_payments_vals()
                for p in payments:
                    try:
                        if p['journal_name']:
                            journal_names = p['journal_name'] + " " + journal_names
                    except:
                        _logger.warning(p['ref'])
                    try:
                        if p['is_transbank_journal']:
                            tbk_amount += p['amount']
                    except:
                        _logger.warning(p['ref'])
                amount_tbk_total += tbk_amount

                worksheet.write(n, 0, rec.date_invoice, period_format)
                worksheet.write(n, 1, rec.document_class_id.name or '', style)
                if self.operation_type in ['Compra', 'Honorarios']:
                    worksheet.write(n, 2, rec.reference or '', style)
                else:
                    worksheet.write(n, 2, int(re.sub('\D', '', rec.number)) or '', style)

                worksheet.write(n, 3, rec.number, style)
                worksheet.write(n, 4, rec.partner_id.commercial_partner_id.document_number or '5.555.555-5', style)
                worksheet.write(n, 5, rec.partner_id.commercial_partner_id.name or 'Consumidor Anónimo', style)
                worksheet.write(n, 6, rec.date_invoice, date_format)
                worksheet.write(n, 7, rec.move_id.date, date_format)
                worksheet.write(n, 8, rec.date_due, date_format)
                sign = rec.type in ['in_refund', 'out_refund'] and -1 or 1
                totals = 0
                totals_sign = 0
                for line in rec.invoice_line_ids:
                    if line.price_tax == 0:
                        totals_sign = totals + line.price_subtotal * sign
                worksheet.write(n, 9, totals_sign, style)
                exempt += totals_sign

                amount_untaxed_sign = rec.amount_untaxed * sign
                worksheet.write(n, 10, amount_untaxed_sign, style)
                amount_untaxed += amount_untaxed_sign

                #solo oculto IVA si es nota de crédito con dos períodos tributarios vencidos

                amount_tax_sign = rec.amount_tax * sign
                amount_tax_not_rec = 0
                sign_amount_total = amount_tax_sign
                for line in rec.tax_line_ids:
                    if line.tax_id.no_rec:
                        amount_tax_not_rec += line.amount_total
                # new line
                if sign > 0:
                    sign_amount_total -= amount_tax_not_rec
                # new line
                worksheet.write(n, 11, sign_amount_total, style)
                amount_tax += sign_amount_total


                amount_total_sign = rec.amount_total * sign
                worksheet.write(n, 12, amount_total_sign, style)
                amount_total += amount_total_sign


                worksheet.write(n, 13, rec.user_id.name, style)
                #Look for Warehouse
                branch = 'Casa Matriz'
                if rec.origin:
                    origin_done = rec.origin.split(',')[0].rstrip() or rec.origin.split(',')[0].lstrip() or rec.origin.origin or rec.origin

                    if self.operation_type not in ['Compra', 'Honorarios']:
                        operation = self.env['sale.order'].sudo().search([('name', '=', origin_done)], limit=1)
                        if warehouse and operation:
                            branch = operation.warehouse_id.name
                    else:
                        operation = self.env['purchase.order'].sudo().search([('name', '=', origin_done)], limit=1)
                        if warehouse and operation:
                            branch = operation.picking_type_id.warehouse_id.name
                worksheet.write(n, 14, branch, style)
                worksheet.write(n, 15, rec.sii_result or 'No Enviado', style)
                worksheet.write(n, 16, rec.state.replace('paid','Pagado').replace('open','Abierto').replace('paid','Pagado').replace('done','Pagado') or "", style)
                worksheet.write(n, 17, journal_names or 0, style)
                worksheet.write(n, 18, tbk_amount or 0, style)
                if self.include_salesperson:
                    if rec.partner_id.user_id:
                        salesperson = rec.partner_id.user_id.name
                        worksheet.write(n, 19, salesperson or '', style)


                n = n+1

        #IF ETD in POS
        if pos_etd_document_obj and self.operation_type in ['Venta']:
            for pos in pos_etd_document_obj:
                journal_names = ""
                tbk_amount = 0
                for p in pos.statement_ids:
                    journal_names = p.journal_id.name + "  " + journal_names
                    if p.journal_id.is_transbank_journal:
                        tbk_amount += p.amount
                amount_tbk_total += tbk_amount

                worksheet.write(n, 0, pos.date_order, period_format)
                worksheet.write(n, 1, pos.document_class_id.name or '', style)
                worksheet.write(n, 2, pos.sii_document_number or '', style)
                worksheet.write(n, 3, pos.account_move.name or pos.session_id.name, style)
                worksheet.write(n, 4, pos.partner_id.commercial_partner_id.document_number or '5.555.555-5', style)
                worksheet.write(n, 5, pos.partner_id.commercial_partner_id.name or 'Consumidor Anónimo', style)
                worksheet.write(n, 6, pos.date_order, date_format)
                worksheet.write(n, 7, pos.date_order, date_format)
                worksheet.write(n, 8, pos.date_order, date_format)
                totals = 0
                    #Look for lines without Tax for exempt
                for line in pos.lines:
                    if not line.tax_ids_after_fiscal_position:
                        totals = totals + line.price_subtotal
                worksheet.write(n, 9, totals, style)
                exempt += totals
                untaxed_calculated = pos.amount_total - pos.amount_tax
                worksheet.write(n, 10, untaxed_calculated or '', style)
                amount_untaxed += untaxed_calculated
                pos_amount_total = pos.amount_tax
                pos_amount_total -= amount_tax_not_rec
                worksheet.write(n, 11, pos_amount_total, style)
                amount_tax += pos_amount_total
                totals = 0
                worksheet.write(n, 12, pos.amount_total, style)
                amount_total += pos.amount_total
                worksheet.write(n, 13, pos.user_id.name or '', style)
                    #Look for Warehouse
                branch = pos.session_id.config_id.name or pos.name
                worksheet.write(n, 14, branch or '', style)
                worksheet.write(n, 15, pos.sii_result or 'No Enviado', style)
                worksheet.write(n, 16, pos.state.replace('paid','Pagado').replace('open','Abierto').replace('paid','Pagado').replace('done','Pagado') or "", style)
                worksheet.write(n, 17, journal_names or 0, style)
                worksheet.write(n, 18, tbk_amount or 0, style)
                if self.include_salesperson:
                    salesperson = pos.partner_id.user_id.name
                    worksheet.write(n, 19, salesperson or '', style)

                n = n+1



        worksheet.write(n, 0, "Total General", style1)
        worksheet.write(n, 1, "", style1)
        worksheet.write(n, 2, "", style1)
        worksheet.write(n, 3, "", style1)
        worksheet.write(n, 4, "", style1)
        worksheet.write(n, 5, "", style1)
        worksheet.write(n, 6, "", style1)
        worksheet.write(n, 7, "", style1)
        worksheet.write(n, 8, "", style1)
        worksheet.write(n, 9, exempt, style1)
        worksheet.write(n, 10, amount_untaxed, style1)
        worksheet.write(n, 11, amount_tax, style1)
        worksheet.write(n, 12, amount_total, style1)
        worksheet.write(n, 13, "", style1)
        worksheet.write(n, 14, "", style1)
        worksheet.write(n, 15, "", style1)
        worksheet.write(n, 16, "", style1)
        worksheet.write(n, 17, "", style1)
        worksheet.write(n, 18, amount_tbk_total, style1)
        if self.include_salesperson:
            worksheet.write(n, 19, "", style1)

        fp = io.BytesIO()
        workbook.save(fp)
        export_id = self.env['etd.document.excel'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'etd.document.excel',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self._context,
            'target': 'new',

        }
        return True

class picking_centralized_excel(models.TransientModel):
    _name= "etd.document.excel"
    _description = 'etd.document.excel'
    excel_file = fields.Binary('Registros de Compra y Venta')
    file_name = fields.Char('Excel File', size=64)
