# Copyright (C) 2021 Konos
from odoo import api, fields, models, _


class AccountPayment(models.Model):
    _inherit = "account.payment"

    is_transbank_journal = fields.Boolean(related="journal_id.is_transbank_journal")