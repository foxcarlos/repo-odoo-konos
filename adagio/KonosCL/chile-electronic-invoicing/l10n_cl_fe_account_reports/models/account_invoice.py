# Copyright (C) 2021 Konos

from odoo import api, models

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def _get_payments_vals(self):
        res = super(AccountInvoice, self)._get_payments_vals()
        for data in res:
            payment_id = data.get('account_payment_id')
            if payment_id:
                payment = self.env['account.payment'].browse(payment_id)
                data.update(is_transbank_journal=payment.journal_id.is_transbank_journal)
        return res
