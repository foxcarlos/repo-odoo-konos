# Copyright (C) 2021 Konos
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)
from odoo import api, fields, models

class AccountInvoice(models.Model):
    _inherit = 'account.journal'

    is_transbank_journal = fields.Boolean("TBK Journal")