# Copyright (C) 2020 Konos
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "Chilean Purchase and Sales Registry",
    "version": "0.0.6",
    "license": "LGPL-3",
    "summary": "Chilean Purchase and Sales Registry",
    "author": "Konos",
    "maintainer": "Konos",
    "website": "https://www.konos.cl",
    "depends": [
        "l10n_cl_fe",
    ],
    "data": [
        "views/account_journal_view.xml",
        "views/account_payment.xml",
        "wizard/etd_account_report.xml",
    ],
    "application": False,
    "sequence": 0,
}
