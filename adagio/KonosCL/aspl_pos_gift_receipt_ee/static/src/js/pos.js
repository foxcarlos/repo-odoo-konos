odoo.define('aspl_pos_gift_receipt_ee.gift_receipt', function (require) {
"use strict";

	var models = require('point_of_sale.models');
	var screens = require('point_of_sale.screens');
	var core = require('web.core');
	var chrome = require('point_of_sale.chrome');
	var gui = require('point_of_sale.gui');
	var PopupWidget = require('point_of_sale.popups');
	
	var QWeb = core.qweb;
	var _t = core._t;

    var GiftReceipt = screens.ActionButtonWidget.extend({
        template: 'GiftReceipt',
        button_click: function(){
            var order    = this.pos.get_order();
            var lines    = order.get_orderlines();
            var selected_line = order.get_selected_orderline();
            order.set_gift_receipt_mode(!order.get_gift_receipt_mode());
            if(order.get_gift_receipt_mode()){
                $(this.el).addClass('highlight');
            } else {
                $(this.el).removeClass('highlight')
            }
            if (selected_line) {
                selected_line.set_line_for_gift_receipt(order.get_gift_receipt_mode());
            }
        },
    });
    screens.define_action_button({
        'name': 'GiftReceipt',
        'widget': GiftReceipt,
        'condition': function(){
            return this.pos.config.enable_gift_receipt;
        },
    });

    var SplitGiftReceipt = screens.ActionButtonWidget.extend({
        template: 'SplitGiftReceipt',
        button_click: function(){
        	var self = this;
            var order    = self.pos.get_order();
            var lines    = order.get_orderlines();
            var flag = false;
            if(lines.length > 0) {
            	lines.map(function(line){
            		if(line.get_line_for_gift_receipt()){
                		flag = true;
                	}
            	})
            	if(flag){
            		self.gui.show_popup('split_product_popup');
            	}else{
            		alert("Please select product for gift receipt.");
            	}
            } else {
                $( ".order-empty" ).effect( "bounce", {}, 500 );
            }
        },
    });
    screens.define_action_button({
        'name': 'SplitGiftReceipt',
        'widget': SplitGiftReceipt,
        'condition': function(){
            return this.pos.config.enable_gift_receipt;
        },
    });

    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function(attr,options){
            _super_orderline.initialize.call(this, attr, options);
        },
        set_line_for_gift_receipt: function(line_for_gift_receipt) {
            this.set('line_for_gift_receipt', line_for_gift_receipt);
        },
        get_line_for_gift_receipt: function() {
            return this.get('line_for_gift_receipt');
        },
        export_for_printing: function(){
            var new_val = {};
            var orderlines = _super_orderline.export_for_printing.call(this);
            new_val = {
            	gift_receipt: this.get_line_for_gift_receipt() || false,
                
            };
            $.extend(orderlines, new_val);
            return orderlines;
        },
        set_line_random_number: function(random_number){
            this.set('line_random_number', random_number);
        },
        get_line_random_number: function(){
            return this.get('line_random_number');
        },
    });

    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function(attributes,options){
            _super_Order.initialize.call(this, attributes,options);
            this.set({
                'gift_receipt_mode': false,
            })
        },
        set_gift_receipt_data: function(receipt_data){
            this.set('receipt_data',receipt_data);
        },
        get_gift_receipt_data: function(){
            return this.get('receipt_data');
        },
        set_gift_receipt_mode: function(gift_receipt_mode){
            this.set('gift_receipt_mode', gift_receipt_mode);
        },
        get_gift_receipt_mode: function() {
            return this.get('gift_receipt_mode');
        },
        set_num_of_line: function(line_length){
            this.set('no_of_lines',line_length);
        },
        get_num_of_line: function(){
            return this.get('no_of_lines');
        },
        add_product: function(product, options){
            var self = this;
            _super_Order.add_product.call(this, product, options);
            var selected_line = self.get_selected_orderline();
            if (selected_line){
                if(self.get_gift_receipt_mode()){
                    selected_line.set_line_for_gift_receipt(self.get_gift_receipt_mode());
                }
            }
        },
        generate_unique_id: function() {
            var timestamp = new Date().getTime();
            return Number(timestamp.toString().slice(-10));
        },
        export_for_printing: function(){
        	var new_val = {};
            var orders = _super_Order.export_for_printing.call(this);
            var order_no = this.get_name() || false;
            if (order_no.indexOf(_t("Order ")) != -1) {
                order_no = order_no.split(_t("Order "));
            }
            new_val = {
            	order_no: order_no[1],
                all_gift_receipt: this.get_gift_receipt_data() || false,
            };
            $.extend(orders, new_val);
            return orders;
        },
    });
    screens.OrderWidget.include({
        click_line: function(orderline, event) {
            var self = this;
            var order = this.pos.get_order();
            this._super(orderline, event);
            var selected_line = order.get_selected_orderline();
            if(selected_line && selected_line.get_line_for_gift_receipt()){
                $('.gift_receipt_btn').addClass('highlight');
            } else {
                $('.gift_receipt_btn').removeClass('highlight');
            }
            order.set_gift_receipt_mode(selected_line.get_line_for_gift_receipt());
        },
        set_value: function(val) {
            this._super(val);
            var order = this.pos.get_order();
            var selected_line = order.get_selected_orderline();
            if (selected_line && selected_line.get_line_for_gift_receipt()) {
                order.set_gift_receipt_mode(true);
                $('.gift_receipt_btn').addClass('highlight');
            } else if(!selected_line || !selected_line.get_line_for_gift_receipt()){
                order.set_gift_receipt_mode(false);
                $('.gift_receipt_btn').removeClass('highlight');
            }
        },
    });

    var PosModel = models.PosModel.prototype;
	models.PosModel = models.PosModel.extend({
		add_new_order: function(){
		    var order = PosModel.add_new_order.call(this);
		    if(order && order.get_gift_receipt_mode()){
		        $('.gift_receipt_btn').addClass('highlight');
		    } else {
		        $('.gift_receipt_btn').removeClass('highlight');
		    }
		    return order;
		},
	});

    chrome.OrderSelectorWidget.include({
        order_click_handler: function(event,$el) {
            this._super(event,$el);
            var order = this.pos.get_order();
            if(order.get_gift_receipt_mode()){
		        $('.gift_receipt_btn').addClass('highlight');
		    } else {
		        $('.gift_receipt_btn').removeClass('highlight');
		    }
        },

    });

    screens.ReceiptScreenWidget.include({
        show: function(){
            this._super();
            var self = this;
            var order = self.pos.get_order();
            if(order && order.get_name()){
            	var barcode_value = order.get_name();
                if (barcode_value.indexOf(_t("Order ")) != -1) {
                    var vals = barcode_value.split(_t("Order "));
                    if (vals) {
                        var barcode = vals[1];
                        $("tr#barcode1").html($("<td text-align:center;><div class='" + barcode + "' /></td>"));
                        $("." + barcode.toString()).barcode(barcode.toString(), "code128");
                        $("td#barcode_val").html(barcode);
                    }
                }
            }

        },
        render_receipt: function() {
            this._super();
            var order = this.pos.get_order();
            var gift_receipt_lines = _.filter(order.get_orderlines(), function(line){
                return line.get_line_for_gift_receipt()
            })
            var gift_receipt_data = order.get_gift_receipt_data();
            if (gift_receipt_data){
                for (var key in gift_receipt_data) {
                    this.$('.pos-receipt-container').append(QWeb.render('PosGiftTicket',{
                        widget:this,
                        order: order,
                        data:gift_receipt_data[key],
                        receipt: order.export_for_printing(),
                        orderlines: gift_receipt_lines,
                        all_gift_receipt: order.get_gift_receipt_data(),
                    }));
                }
            }
        },
    });

    /* Split Product POPUP */
	var SplitProductPopup = PopupWidget.extend({
	    template: 'SplitProductPopup',
	    show: function(options){
	    	var self = this;
			this._super();
			var order    = self.pos.get_order();
            var lines    = order.get_orderlines();
			self.line_list = [];
            if(lines.length > 0) {
            	lines.map(function(line){
            		if(line && line.get_line_for_gift_receipt()){
            			self.line_list.push(line);
            		}
            	});
                order.set_num_of_line(self.line_list.length);
            }
			this.renderElement();
	    },
	    click_confirm: function(){
	    	var self = this;
	        this.gui.close_popup();
	    },
	    renderElement: function() {
            var self = this;
            this._super();
            var dict_data = {};
            var order = self.pos.get_order();
            if( order && order.get_gift_receipt_data()){
                $('#total_receipt').text("Total Gift Receipt: "+order.get_gift_receipt_data().length);
            } else{
                $('#total_receipt').text("Total Gift Receipt: "+'0');
            }
            $('div.split').click(function(){
                dict_data = {};
            	var rand_number = Math.floor(Math.random() * 1000);
            	$.each($("input[class='select_chkbox']"), function(key,value){
                    if(!value){
                        return
                    }
                    var id = $(value).data('id');
                    var row = $('.popup-product-list-contents tr[data-id="'+ id +'"]');
                    if(value.checked){
                        if($(row).find("input.rand_number_text").val() == ""){
                            $(row).find("input.rand_number_text").val(rand_number);
                            var random_num = row.find("input.rand_number_text").val();
                        }
            		} else{
                        $(row).find("input.rand_number_text").val('');
                    }
         
    	    	});
                $.each($("input[class='select_chkbox']"), function(key,value){
                    if(value && value.checked){
                        var id = $(value).data('id');
                        var row = $('.popup-product-list-contents tr[data-id="'+ id +'"]');
                        var random_num = row.find("input.rand_number_text").val();
                        var orderline = order.get_orderline(id);
                        !dict_data.hasOwnProperty(random_num) ? dict_data[random_num]=[orderline] : dict_data[random_num].push(orderline);
                        orderline.set_line_random_number(random_num);
                    } else{
                        var id = $(value).data('id');
                        var row = $('.popup-product-list-contents tr[data-id="'+ id +'"]');
                        var random_num = row.find("input.rand_number_text").val();
                        var orderline = order.get_orderline(id);
                        orderline.set_line_random_number(random_num);
                    }

                    $('#total_receipt').text("Total Gift Receipt: "+Object.keys(dict_data).length);
                });
                var parent_list = [];
                var temp_data_list = [];
                $.each(dict_data, function(key,value){
                    var temp_line_list = [];
                    $.each(value, function(key,line){
                        var temp_data_list = []
                        temp_data_list.push(line.product.display_name);
                        if(line.product.uom_id[1] !== "Unit(s)"){
                            temp_data_list.push(line.quantityStr+' '+line.product.uom_id[1]);
                        } else{
                            temp_data_list.push(line.quantityStr);
                        }
                        temp_data_list.push(line.quantityStr);
                        temp_line_list.push(temp_data_list);
                    });
                    parent_list.push(temp_line_list);
                });
                order.set_gift_receipt_data(parent_list);
        	});
    	},
    	click_cancel: function(){
    		this.gui.close_popup();
    	},
	});
	gui.define_popup({name:'split_product_popup', widget: SplitProductPopup});

});