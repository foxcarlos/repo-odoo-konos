# -*- coding: utf-8 -*-
{
    'name': "Adagio",

    'summary': """App for customizations""",

    'author': "Konos Soluciones & Servicios",
    'website': "https://www.konos.cl",

    'category': 'Uncategorized',
    'version': '12.0.0.0.0',

    'depends': [
        'crm',
        'mrp',
        'shopify_ept',

        'l10n_cl_fe',
        'l10n_cl_stock_picking',
        'l10n_cl_dte_point_of_sale',
    ],

    'data': [
        'data/res_config.xml',
        'data/sii_activity_desc.xml',
        'data/res_company.xml',
    ],

    'demo': [
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
