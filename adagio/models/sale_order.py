# -*- coding: utf-8 -*-

from odoo import models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def action_invoice_create(self, grouped=False, final=False):
        """Currently, sale orders created in Odoo through Shopify can contain
        negative amounts if a discount has been applied, although this behavior
        is not wrong, it can cause some troubles when validating the invoice
        according to Chilean regulations.
        This hack allows us to remove these negative discounts from invoices
        and unify them as a global discount.
        """
        invoice_ids = super(SaleOrder, self).action_invoice_create()

        target_country = self.env.ref('base.cl')
        current_country = self.env.user.company_id.country_id

        if self.shopify_instance_id and current_country == target_country:
            invoices = self.env['account.invoice'].browse(invoice_ids)

            for invoice in invoices:
                lines = invoice.invoice_line_ids.filtered(
                    lambda r: r.product_id == self.shopify_instance_id.discount_product_id
                )
                if lines:
                    discount = abs(sum(lines.mapped('price_subtotal')))
                    invoice.write({
                        'global_descuentos_recargos': [(0, 0, {
                            'type': 'D',
                            'gdr_type': 'amount',
                            'valor': discount,
                            'gdr_detail': 'Shopify Discount',
                        })]
                    })
                    lines.unlink()

                # Set the document type as 'Boleta Electronica' by default
                bel = self.env.ref('l10n_cl_fe.dc_b_f_dte')

                # REVIEW: we can have several journals associated with the
                # document type 'Boleta Electronica' by example Sale, POS.
                # In this case, we only need the journal related to sales
                # that usually is the first to be created..
                document_class = self.env['account.journal.sii_document_class'].search([
                    ('journal_id.type', '=', 'sale'),
                    ('sii_document_class_id.sii_code', '=', bel.sii_code)
                ], limit=1)

                invoice.write({
                    'journal_document_class_id': document_class.id,
                })

        return invoice_ids
